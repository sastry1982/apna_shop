    </div>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/vendor/angular.min.js"></script>
    <script type="text/javascript" src="/vendor/angular-animate.min.js"></script>
    <script type="text/javascript" src="/vendor/angular-aria.min.js"></script>
    <script type="text/javascript" src="/vendor/angular-messages.min.js"></script>
    <script type="text/javascript" src="/vendor/angular-route.min.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
</body>
</html>