<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="Apna Shop">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Apna Shop</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" href="/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/css/main.css"/>

    <script type="text/javascript" src="/js/jquery.min.js"></script>
</head>
<body>
<header class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Apna Shop</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#">About</a>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if (!empty($_SESSION['MEMBER_ID'])) { ?>
                    <li><a href="#">My Account</a></li>
                    <?php if ($_SESSION['MEMBER_TYPE'] == "SHOP_OWNER") { ?>
                        <li><a href="/shop/">My Shop</a></li>
                    <?php } ?>
                    <li><a href="/home/logout/">Logout</a></li>
                <?php } else { ?>
                    <!--li><a href="/home/signup/">Sign up</a></li>
                    <li><a href="/login/">Login</a></li-->
                    <li><a href="#/register">Register</a></li>
                    <li><a href="#/login">Login</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</header>
<div class="container master-container" ng-app="ApnaShopApp">
    <?php getMessage(); ?>

