<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Aj extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->load->model("DBAPI", "dbapi", TRUE);
    }

    public function index() {
        $data = array();
        $this->_template("home", $data);
    }


}
