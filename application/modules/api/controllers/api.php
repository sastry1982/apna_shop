<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->_admin_login_check();
        $this->load->model("AdminModel", "admin", TRUE);
    }

    public function index() {
        $data = array();
        $this->_template('admin/dashboard', $data);
    }

    public function dashboard() {
        $data = array();
        $this->_template('admin/dashboard', $data);
    }

}
