<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Locations extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->load->model("AdminModel", "admin", TRUE);
        $this->_admin_login_check();
    }

    public function index() {
        $data = array();
        if (!empty($_GET['act']) && $_GET['act'] == "del" && !empty($_GET['location_id'])) {
            $this->admin->delLocation($_GET['location_id']);
            redirect(base_url() . "admin/locations");
        }
        if (!empty($_GET['act']) && $_GET['act'] == "status" && !empty($_GET['location_id']) && isset($_GET['sta'])) {
            $is_active = (!empty($_GET['sta']) && $_GET['sta'] == "1") ? "1" : "0";
            $this->admin->updateLocation(array("is_active" => $is_active), $_GET['location_id']);
            redirect(base_url() . "admin/locations");
        }
        $search_data = array();
        if (!empty($this->_REQ['key'])) {
            $search_data['key'] = $this->_REQ['key'];
        }
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = $search_data;
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/admin/locations?';
        $this->pagenavi->process($this->admin, 'searchLocations');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['locations'] = $this->pagenavi->items;
        $this->_template('locations/index', $data);
    }

    public function add() {
        $data = array();
        if (!empty($_POST['location_name'])) {
            $pdata = array();
            $pdata['location_name'] = !empty($_POST['location_name']) ? trim($_POST['location_name']) : "";
            $pdata['parent_id'] = isset($_POST['parent_id']) ? trim($_POST['parent_id']) : "0";
            $this->admin->addLocation($pdata);
            redirect(base_url() . "admin/locations");
        }
        $data['parent_locations'] = $this->admin->getLocationsList("0", "1");
        $this->_template('locations/form', $data);
    }

    public function edit() {
        $data = array();
        if (!empty($_POST['location_id'])) {
            $pdata = array();
            $pdata['location_name'] = !empty($_POST['location_name']) ? trim($_POST['location_name']) : "";
            $pdata['parent_id'] = isset($_POST['parent_id']) ? trim($_POST['parent_id']) : "0";
            $this->admin->updateLocation($pdata, $_POST['location_id']);
            redirect(base_url() . "admin/locations");
        }
        $data['location'] = $this->admin->getLocationById($this->_REQ['location_id']);
        $data['parent_locations'] = $this->admin->getLocationsList("0", "1");
        $this->_template('locations/form', $data);
    }


}