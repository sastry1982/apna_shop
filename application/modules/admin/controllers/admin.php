<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->_admin_login_check();
        $this->load->model("AdminModel", "admin", TRUE);
    }

    public function index() {
        $data = array();
        $this->_template('admin/dashboard', $data);
    }

    public function dashboard() {
        $data = array();
        $this->_template('admin/dashboard', $data);
    }

    public function profile() {
        $data = array();
        $data['admin'] = $this->admin->getAdminById($_SESSION['ADMIN_ID']);
        if (!empty($_POST['frm_profile']) && !empty($_POST['admin_id'])) {
            $pdata = array();
            $pdata['name'] = !empty($_POST['name']) ? trim($_POST['name']) : "";
            $this->admin->updateAdmin($_SESSION['ADMIN_ID'], $pdata);
            $_SESSION['message'] = "Your Profile has been updated successfully.";
            redirect(base_url() . "admin/profile/");
        }
        if (!empty($_POST['frm_pwd']) && !empty($_POST['admin_id'])) {
            $pdata = array();
            $pdata['pwd'] = !empty($_POST['pwd']) ? trim($_POST['pwd']) : "";
            $this->admin->updateAdmin($_SESSION['ADMIN_ID'], $pdata);
            $_SESSION['message'] = "Your Password has been updated successfully.";
            redirect(base_url() . "admin/profile/");
        }
        $this->_template('profile', $data);
    }

    public function logout() {
        redirect(base_url() . "admin/login/");
    }

}
