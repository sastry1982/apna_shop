<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Members extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->load->model("AdminModel", "admin", TRUE);
        $this->_admin_login_check();
    }

    public function index() {
        $data = array();
        if (!empty($_GET['act']) && $_GET['act'] == "del" && !empty($_GET['member_id'])) {
            $this->admin->delMember($_GET['member_id']);
            redirect(base_url() . "admin/members/");
        }
        if (!empty($_GET['act']) && $_GET['act'] == "status" && !empty($_GET['member_id']) && isset($_GET['sta'])) {
            $is_active = (!empty($_GET['sta']) && $_GET['sta'] == "1") ? "1" : "0";
            $this->admin->updateMember(array("is_active" => $is_active), $_GET['member_id']);
            redirect(base_url() . "admin/members/");
        }
        $search_data = array();
        if (!empty($this->_REQ['key'])) {
            $search_data['key'] = $this->_REQ['key'];
        }
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = $search_data;
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/admin/members/?';
        $this->pagenavi->process($this->admin, 'searchMembers');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['members'] = $this->pagenavi->items;
        $this->_template('members/index', $data);
    }

    public function add() {
        $data = array();
        if (!empty($_POST['name'])) {
            $pdata = array();
            $pdata['name'] = !empty($_POST['name']) ? trim($_POST['name']) : "";
            $this->admin->addMember($pdata);
            redirect(base_url() . "admin/members/");
        }
        $data['locations'] = [];
        $data['parent_locations'] = $this->dbapi->getLocationsList("0", "1");
        if (!empty($data['parent_locations'])) {
            foreach ($data['parent_locations'] as $loc_id => $loc) {
                $data['locations'] = $this->dbapi->getLocationsList($loc_id, "1");
                break;
            }
        }
        $data['categories'] = $this->admin->getCategoriesList();
        $this->_template('members/form', $data);
    }

    public function edit() {
        $data = array();
        if (!empty($_POST['member_id'])) {
            $pdata = array();
            $pdata['name'] = !empty($_POST['name']) ? trim($_POST['name']) : "";
            $pdata['gender'] = !empty($_POST['gender']) ? trim($_POST['gender']) : "";
            $pdata['email'] = !empty($_POST['email']) ? trim($_POST['email']) : "";
            $pdata['mobile'] = !empty($_POST['mobile']) ? trim($_POST['mobile']) : "";
            $pdata['location_id'] = !empty($_POST['location_id']) ? trim($_POST['location_id']) : "";
            $pdata['member_type'] = !empty($_POST['member_type']) ? trim($_POST['member_type']) : "";
            $this->admin->updateMember($pdata, $_POST['member_id']);
            if ($pdata['member_type'] == "SHOP_OWNER") {
                $shop = array();
                $shop['owner_id'] = $_POST['member_id'];
                $shop['shop_name'] = !empty($_POST['shop_name']) ? $_POST['shop_name'] : "";
                $shop['shop_phone'] = !empty($_POST['shop_phone']) ? $_POST['shop_phone'] : "";
                $shop['category_id'] = !empty($_POST['category_id']) ? $_POST['category_id'] : "";
                $shop['city_id'] = !empty($_POST['city_id']) ? $_POST['city_id'] : "";
                $shop['location_id'] = !empty($_POST['location_id']) ? $_POST['location_id'] : "";
                $shop['shop_address'] = !empty($_POST['shop_address']) ? $_POST['shop_address'] : "";
                if (!empty($_POST['shop_id'])) {
                    $img = imgUpload('shop_logo', '/data/logos/', 'logo' . $_POST['shop_id']);
                    if ($img !== false) {
                        $shop['shop_logo'] = $img;
                    }
                    $this->admin->updateShop($shop, $_POST['shop_id']);
                } else {
                    $shop_id = $this->admin->addShop($shop);
                    $img = imgUpload('shop_logo', '/data/logos/', 'logo' . $shop_id);
                    if ($img !== false) {
                        $this->admin->updateShop(['shop_logo' => $img], $shop_id);
                    }
                }
            }
            redirect(base_url() . "admin/members/");
        }
        $data['locations'] = [];
        $data['member'] = $this->admin->getMemberById($this->_REQ['member_id']);
        $data['shop'] = $this->admin->getShopByOwnerId($this->_REQ['member_id']);
        $data['parent_locations'] = $this->admin->getLocationsList("0", "1");
        if (!empty($data['parent_locations'])) {
            $city_id = !empty($data['member']['city_id']) ? $data['member']['city_id'] : "";
            $city_id = !empty($data['shop']['city_id']) ? $data['shop']['city_id'] : $city_id;
            if (empty($city_id)) {
                foreach ($data['parent_locations'] as $loc_id => $loc) {
                    $city_id = $loc_id;
                    break;
                }
            }
            $data['locations'] = $this->admin->getLocationsList($city_id, "1");
        }
        $data['categories'] = $this->admin->getCategoriesList();
        $this->_template('members/form', $data);
    }

}
