<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->load->model("AdminModel", "admin", TRUE);
        $this->_admin_login_check();
    }

    public function index() {
        $data = array();
        if (!empty($_GET['act']) && $_GET['act'] == "del" && !empty($_GET['category_id'])) {
            $this->admin->delCategory($_GET['category_id']);
            redirect(base_url() . "admin/categories/");
        }
        if (!empty($_GET['act']) && $_GET['act'] == "status" && !empty($_GET['category_id']) && isset($_GET['sta'])) {
            $is_active = (!empty($_GET['sta']) && $_GET['sta'] == "1") ? "1" : "0";
            $this->admin->updateCategory(array("is_active" => $is_active), $_GET['category_id']);
            redirect(base_url() . "admin/categories/");
        }
        $search_data = array();
        if (!empty($this->_REQ['key'])) {
            $search_data['key'] = $this->_REQ['key'];
        }
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = $search_data;
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/admin/categories/?';
        $this->pagenavi->process($this->admin, 'searchCategories');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['categories'] = $this->pagenavi->items;
        $this->_template('categories/index', $data);
    }

    public function add() {
        $data = array();
        if (!empty($_POST['category_name'])) {
            $pdata = array();
            $pdata['category_name'] = !empty($_POST['category_name']) ? trim($_POST['category_name']) : "";
            //$pdata['parent_id'] = !empty($_POST['parent_id']) ? trim($_POST['parent_id']) : "0";
            $this->admin->addCategory($pdata);
            redirect(base_url() . "admin/categories/");
        }
        $this->_template('categories/form', $data);
    }

    public function edit() {
        $data = array();
        $data['category'] = $this->admin->getCategoryById($this->_REQ['category_id']);
        if (!empty($_POST['category_id'])) {
            $pdata = array();
            $pdata['category_name'] = !empty($_POST['category_name']) ? trim($_POST['category_name']) : "";
            //$pdata['parent_id'] = !empty($_POST['parent_id']) ? trim($_POST['parent_id']) : "0";
            $this->admin->updateCategory($pdata, $_POST['category_id']);
            redirect(base_url() . "admin/categories/");
        }
        $this->_template('categories/form', $data);
    }


}