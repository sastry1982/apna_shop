<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shops extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->load->model("AdminModel", "admin", TRUE);
        $this->_admin_login_check();
    }

    public function index() {
        $data = array();
        if (!empty($_GET['act']) && $_GET['act'] == "del" && !empty($_GET['shop_id'])) {
            $this->admin->delShop($_GET['shop_id']);
            redirect(base_url() . "admin/shops/");
        }
        if (!empty($_GET['act']) && $_GET['act'] == "status" && !empty($_GET['shop_id']) && isset($_GET['sta'])) {
            $is_active = (!empty($_GET['sta']) && $_GET['sta'] == "1") ? "1" : "0";
            $this->admin->updateShop(array("is_active" => $is_active), $_GET['shop_id']);
            redirect(base_url() . "admin/shops/");
        }
        $search_data = array();
        if (!empty($this->_REQ['key'])) {
            $search_data['key'] = $this->_REQ['key'];
        }
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = $search_data;
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/admin/shops/?';
        $this->pagenavi->process($this->admin, 'searchShops');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['shops'] = $this->pagenavi->items;
        $this->_template('shops/index', $data);
    }

    public function add() {
        $data = array();
        if (!empty($_POST['shop_name'])) {
            $pdata = array();
            $pdata['shop_name'] = !empty($_POST['shop_name']) ? trim($_POST['shop_name']) : "";
            $pdata['owner_id'] = !empty($_POST['owner_id']) ? trim($_POST['owner_id']) : "";
            $pdata['shop_logo'] = !empty($_POST['shop_logo']) ? trim($_POST['shop_logo']) : "";
            $pdata['shop_phone'] = !empty($_POST['shop_phone']) ? trim($_POST['shop_phone']) : "";
            $pdata['shop_address'] = !empty($_POST['shop_address']) ? trim($_POST['shop_address']) : "";
            $pdata['shop_description'] = !empty($_POST['shop_description']) ? trim($_POST['shop_description']) : "";
            $shop_id = $this->admin->addShop($pdata);
            $img = imgUpload('shop_logo', '/data/logos/', 'logo' . $shop_id);
            if ($img !== false) {
                $this->admin->updateShop(['shop_logo' => $img], $shop_id);
            }
            redirect(base_url() . "admin/shops/");
        }
        $data['categories'] = $this->admin->getCategoriesList();
        $this->_template('shops/form', $data);
    }

    public function edit() {
        $data = array();
        if (!empty($_POST['shop_id'])) {
            $pdata = array();
            $pdata['shop_name'] = !empty($_POST['shop_name']) ? trim($_POST['shop_name']) : "";
            //$pdata['owner_id'] = !empty($_POST['owner_id']) ? trim($_POST['owner_id']) : "";            
            $pdata['shop_phone'] = !empty($_POST['shop_phone']) ? trim($_POST['shop_phone']) : "";
            $pdata['category_id'] = !empty($_POST['category_id']) ? trim($_POST['category_id']) : "";
            $pdata['city_id'] = !empty($_POST['city_id']) ? trim($_POST['city_id']) : "";
            $pdata['location_id'] = !empty($_POST['location_id']) ? trim($_POST['location_id']) : "";
            $pdata['shop_address'] = !empty($_POST['shop_address']) ? trim($_POST['shop_address']) : "";
            $pdata['shop_description'] = !empty($_POST['shop_description']) ? trim($_POST['shop_description']) : "";
            $img = imgUpload('shop_logo', '/data/logos/', 'logo' . $_POST['shop_id']);
            if ($img !== false) {
                $pdata['shop_logo'] = $img;
            }
            $this->admin->updateShop($pdata, $_POST['shop_id']);
            redirect(base_url() . "admin/shops/");
        }
        $data['shop'] = $this->admin->getShopById($this->_REQ['shop_id']);
        $data['parent_locations'] = $this->admin->getLocationsList("0", "1");
        if (!empty($data['parent_locations'])) {
            $city_id = !empty($data['shop']['city_id']) ? $data['shop']['city_id'] : "";
            if (empty($city_id)) {
                foreach ($data['parent_locations'] as $loc_id => $loc) {
                    $city_id = $loc_id;
                    break;
                }
            }
            $data['locations'] = $this->admin->getLocationsList($city_id, "1");
        }
        $data['categories'] = $this->admin->getCategoriesList();
        $this->_template('shops/form', $data);
    }

}
