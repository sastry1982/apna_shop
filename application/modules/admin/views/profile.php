<div class="container">
    <section class="box box-primary">
        <article class="box-body">
            <h3 class="col-sm-4 padding-left page-header">Update Profile</h3>
            <div class="clearfix"></div>
            <form id="frm" class="form-horizontal" role="form" method="post">
                <input type="hidden" name="frm_profile" id="frm_profile" value="true"/>
                <input type="hidden" name="admin_id" id="admin_id" value="<?php echo!empty($admin['admin_id']) ? $admin['admin_id'] : ""; ?>"/>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-3"> <?php echo!empty($admin['email']) ? $admin['email'] : ""; ?> </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control required" id="name" name="name" placeholder="First Name" value="<?php echo!empty($admin['name']) ? $admin['name'] : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </form>
            <div class="h1 clearfix"></div>
            <h3 class="col-sm-4 page-header padding-left">Update Password</h3>
            <div class="clearfix"></div>
            <form id="pwdfrm" class="form-horizontal" role="form" method="post">
                <input type="hidden" name="frm_pwd" id="frm_pwd" value="true"/>
                <input type="hidden" name="admin_id" id="admin_id" value=""/>
                <div class="form-group">
                    <label for="pwd" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-3">
                        <input type="password" class="form-control required" id="pwd" name="pwd" placeholder="" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="cnfpwd" class="col-sm-2 control-label">Re-Password</label>
                    <div class="col-sm-3">
                        <input type="password" class="form-control required" id="cnfpwd" name="cnfpwd" placeholder="" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </form>
            <div class="clear"></div>
            <div class="h1 clearfix"></div>
        </article>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#frm").validate({
            rules: {
                name: {required: true},
                last_name: {required: true}
            },
            messages: {
                name: {required: "Please Enter your First name"},
                last_name: {required: "Please Enter your Last name"}
            }
        });
        $("#pwdfrm").validate({
            rules: {
                pwd: {required: true},
                cnfpwd: {required: true, equalTo: "#pwd"}
            },
            messages: {
                pwd: {required: "Please Enter Password"},
                cnfpwd: {required: "Please Enter Re-Password"}
            }
        });
    });
</script>
