<div class="container">
    <div class="page-header">
        <h1> <div class="small-head"><?php echo!empty($location['location_id']) ? "Edit" : "Add"; ?> Location</div> </h1>
    </div>
    <form id="frm" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="location_id" id="location_id" value="<?php echo!empty($location['location_id']) ? $location['location_id'] : ""; ?>" />
        <div class="form-group">
            <label for="parent_id" class="col-sm-2 control-label">Main Location</label>
            <div class="col-sm-3">
                <select name="parent_id" id="parent_id" class="form-control required">
                    <option value="0">None</option>
                    <?php if(!empty($parent_locations)){ ?>
                        <?php
                        foreach($parent_locations as $loc_id=>$loc){
                            if(!empty($location['parent_id']) && $location['parent_id']==$loc_id){
                                echo '<option value="'.$loc_id.'" selected="true">'.$loc.'</option>';
                            }else{
                                echo '<option value="'.$loc_id.'">'.$loc.'</option>';
                            }
                        }
                        ?>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="location_name" class="col-sm-2 control-label">Location Name</label>
            <div class="col-sm-3">
                <input type="text" class="form-control required" id="location_name" name="location_name" placeholder="Location Name" value="<?php echo!empty($location['location_name']) ? $location['location_name'] : ""; ?>" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default"><?php echo!empty($location['location_id']) ? "Update" : "Submit"; ?></button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $().ready(function () {
        $("#frm").validate();
    });
</script>
<hr/>