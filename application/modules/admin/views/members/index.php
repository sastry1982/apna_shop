<div class="container">
    <div class="page-header">
        <h1><div class="small-head">Member</div></h1>
    </div>
    <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Key" name="key" value="<?php echo!empty($_GET['key']) ? $_GET['key'] : ""; ?>" />
        </div>
        <button type="submit" class="btn btn-primary">Search</button>
    </form> <br/>
    <!--div class="">
        <a href="/admin/members/add/" class="btn btn-info pull-right">Add Member</a>
    </div-->
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Member Name</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Added On</th>
            <th width="80px">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php if (!empty($members)) { ?>
            <?php foreach ($members as $member) { ?>
                <tr>
                    <td><?php echo $member['member_id']; ?></td>
                    <td><?php echo $member['name']; ?></td>
                    <td><?php echo $member['email']; ?></td>
                    <td><?php echo $member['mobile']; ?></td>
                    <td><?php echo dateTimeDB2SHOW($member['added_on']); ?></td>
                    <td>
                        <a href="/admin/members/edit/?member_id=<?php echo $member['member_id']; ?>" title='Edit'><i class="glyphicon glyphicon-edit"></i></a>
                        <a href="/admin/members/?act=status&member_id=<?php echo $member['member_id']; ?>&sta=<?php echo ($member['is_active'] == "1") ? "0" : "1"; ?>" title='<?php echo ($member['is_active'] == "1") ? "Active" : "Inactive"; ?>'><i class="glyphicon glyphicon-star <?php echo ($member['is_active'] == "1") ? "" : "grey"; ?>"></i></a>
                        <a href="/admin/members/?act=del&member_id=<?php echo $member['member_id']; ?>" title='Delete' onclick="return window.confirm('Do You Want to Delete?');" ><i class="glyphicon glyphicon-trash"></i></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="3">No Members found.</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php echo!empty($PAGING) ? $PAGING : ""; ?>
</div>