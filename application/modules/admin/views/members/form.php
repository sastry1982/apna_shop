<div class="container">
    <div class="page-header">
        <h1>
            <div class="small-head"><?php echo !empty($member['member_id']) ? "Edit" : "Add"; ?> Member</div>
        </h1>
    </div>
    <form id="frm" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="member_id" id="member_id"
               value="<?php echo !empty($member['member_id']) ? $member['member_id'] : ""; ?>"/>

        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Member Name</label>

            <div class="col-sm-3">
                <input type="text" class="form-control required" id="name" name="name" placeholder="Member Name"
                       value="<?php echo !empty($member['name']) ? $member['name'] : ""; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="gender" class="col-sm-2 control-label">Gender</label>

            <div class="col-sm-3">
                <select name="gender" id="gender" class="form-control required">
                    <option
                        value="Male" <?php echo (!empty($member['gender']) && $member['gender'] == "Male") ? 'selected="true"' : ''; ?>>
                        Male
                    </option>
                    <option
                        value="Female" <?php echo (!empty($member['gender']) && $member['gender'] == "Female") ? 'selected="true"' : ''; ?>>
                        Female
                    </option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email</label>

            <div class="col-sm-3">
                <input type="text" class="form-control required" id="email" name="email" placeholder="Email"
                       value="<?php echo !empty($member['email']) ? $member['email'] : ""; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="mobile" class="col-sm-2 control-label">Mobile</label>

            <div class="col-sm-3">
                <input type="text" class="form-control required" id="mobile" name="mobile" placeholder="Mobile"
                       value="<?php echo !empty($member['mobile']) ? $member['mobile'] : ""; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="member_type" class="col-sm-2 control-label">Member Type</label>

            <div class="col-sm-3">
                <select name="member_type" id="member_type" class="form-control required">
                    <option
                        value="CUSTOMER" <?php echo (!empty($member['member_type']) && $member['member_type'] == "CUSTOMER") ? 'selected="true"' : ''; ?>>
                        Customer
                    </option>
                    <option
                        value="SHOP_OWNER" <?php echo (!empty($member['member_type']) && $member['member_type'] == "SHOP_OWNER") ? 'selected="true"' : ''; ?>>
                        Shop Owner
                    </option>
                </select>
            </div>
        </div>
        <div id="shop_block"
             style="<?php echo (!empty($member['member_type']) && $member['member_type'] == "CUSTOMER") ? 'display: none;' : ''; ?>">
            <input type="hidden" name="shop_id" id="shop_id"
                   value="<?php echo !empty($shop['shop_id']) ? $shop['shop_id'] : ""; ?>"/>

            <div class="form-group">
                <label for="shop_name" class="col-sm-2 control-label">Shop Name</label>

                <div class="col-sm-3">
                    <input type="text" class="form-control required" id="shop_name" name="shop_name"
                           placeholder="Shop Name"
                           value="<?php echo !empty($shop['shop_name']) ? $shop['shop_name'] : ""; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="shop_logo" class="col-sm-2 control-label">Shop Logo</label>

                <div class="col-sm-3">
                    <?php
                    if (!empty($shop['shop_logo']) && file_exists(DOC_ROOT_PATH . "/data/logos/" . $shop['shop_logo'])) {
                        echo '<img src="/data/logos/' . $shop['shop_logo'] . '" height="100px" /><br/>';
                    }
                    ?>
                    <input type="file" class="form-control" id="shop_logo" name="shop_logo" value=""/>
                </div>
            </div>
            <div class="form-group">
                <label for="shop_phone" class="col-sm-2 control-label">Shop Phone</label>

                <div class="col-sm-3">
                    <input type="text" class="form-control" id="shop_phone" name="shop_phone" placeholder="Shop Phone"
                           value="<?php echo !empty($shop['shop_phone']) ? $shop['shop_phone'] : ""; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="category_id" class="col-sm-2 control-label">Category</label>
                <div class="col-sm-3">
                    <select name="category_id" id="category_id" class="form-control required">
                        <option value="">select</option>
                        <?php if (!empty($categories)) { ?>
                            <?php
                            foreach ($categories as $cat_id => $cat) {
                                if (!empty($shop['category_id']) && $shop['category_id'] == $cat_id) {
                                    echo '<option value="' . $cat_id . '" selected="true">' . $cat . '</option>';
                                } else {
                                    echo '<option value="' . $cat_id . '">' . $cat . '</option>';
                                }
                            }
                            ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="city" class="col-sm-2 control-label">City</label>
                <div class="col-sm-3">
                    <select name="city_id" id="city" class="form-control required">
                        <?php if (!empty($parent_locations)) { ?>
                            <?php
                            foreach ($parent_locations as $loc_id => $loc) {
                                if (!empty($shop['city_id']) && $shop['city_id'] == $loc_id) {
                                    echo '<option value="' . $loc_id . '" selected="true">' . $loc . '</option>';
                                } else {
                                    echo '<option value="' . $loc_id . '">' . $loc . '</option>';
                                }
                            }
                            ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="location_id" class="col-sm-2 control-label">Area</label>

                <div class="col-sm-3">
                    <select name="location_id" id="location_id" class="form-control required">
                        <?php if (!empty($locations)) { ?>
                            <?php
                            foreach ($locations as $loc_id => $loc) {
                                if (!empty($shop['location_id']) && $shop['location_id'] == $loc_id) {
                                    echo '<option value="' . $loc_id . '" selected="true">' . $loc . '</option>';
                                } else {
                                    echo '<option value="' . $loc_id . '">' . $loc . '</option>';
                                }
                            }
                            ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="shop_address" class="col-sm-2 control-label">Shop Address</label>

                <div class="col-sm-3">
                    <textarea class="form-control required" id="shop_address"
                              name="shop_address"><?php echo !empty($shop['shop_address']) ? $shop['shop_address'] : ""; ?></textarea>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default"><?php echo !empty($member['member_id']) ? "Update" : "Submit"; ?></button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $().ready(function () {
        $("#frm").validate();
        $("#member_type").on("change", function () {

            if ($(this).val() == "SHOP_OWNER") {
                $("#shop_block").show();
            } else {
                $("#shop_block").hide();
            }
        });
        $("#city").on("change", function () {
            $("#location_id").html("");
            $.ajax({
                type: 'POST',
                url: "/home/areas/" + $("#city").val(),
                dataType: "json",
                cache: false,
                success: function (data) {

                    $.each(data, function (key, value) {
                        $('#location_id').append($("<option></option>").attr("value", key).text(value));
                    });
                }
            });
        });
    });
</script>
<hr/>