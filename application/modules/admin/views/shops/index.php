<div class="container">
    <div class="page-header">
        <h1><div class="small-head">Shops</div></h1>
    </div>
    <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Key" name="key" value="<?php echo!empty($_GET['key']) ? $_GET['key'] : ""; ?>" />
        </div>
        <button type="submit" class="btn btn-primary">Search</button>
    </form> <br/>
    <!--div class="">
        <a href="/admin/shops/add/" class="btn btn-info pull-right">Add Shop</a>
    </div-->
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Shop Name</th>
            <th width="80px">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php if (!empty($shops)) { ?>
            <?php foreach ($shops as $shop) { ?>
                <tr>
                    <td><?php echo $shop['shop_id']; ?></td>
                    <td><?php echo $shop['shop_name']; ?></td>
                    <td>
                        <a href="/admin/shops/edit/?shop_id=<?php echo $shop['shop_id']; ?>" title='Edit'><i class="glyphicon glyphicon-edit"></i></a>
                        <a href="/admin/shops/?act=status&shop_id=<?php echo $shop['shop_id']; ?>&sta=<?php echo ($shop['is_active'] == "1") ? "0" : "1"; ?>" title='<?php echo ($shop['is_active'] == "1") ? "Active" : "Inactive"; ?>'><i class="glyphicon glyphicon-star <?php echo ($shop['is_active'] == "1") ? "" : "grey"; ?>"></i></a>
                        <a href="/admin/shops/?act=del&shop_id=<?php echo $shop['shop_id']; ?>" title='Delete' onclick="return window.confirm('Do You Want to Delete?');" ><i class="glyphicon glyphicon-trash"></i></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="3">No Categories found.</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php echo!empty($PAGING) ? $PAGING : ""; ?>
</div>