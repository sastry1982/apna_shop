<div class="container">
    <div class="page-header">
        <h1> <div class="small-head"><?php echo!empty($category['category_id']) ? "Edit" : "Add"; ?> Category</div> </h1>
    </div>
    <form id="frm" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="category_id" id="category_id" value="<?php echo!empty($category['category_id']) ? $category['category_id'] : ""; ?>" />
        <div class="form-group">
            <label for="category_name" class="col-sm-2 control-label">Category Name</label>
            <div class="col-sm-3">
                <input type="text" class="form-control required" id="category_name" name="category_name" placeholder="Category Name" value="<?php echo!empty($category['category_name']) ? $category['category_name'] : ""; ?>" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default"><?php echo!empty($category['category_id']) ? "Update" : "Submit"; ?></button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $().ready(function () {
        $("#frm").validate();
    });
</script>
<hr/>