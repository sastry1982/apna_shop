<div class="container">
    <div class="page-header">
        <h1><div class="small-head">Category</div></h1>
    </div>
    <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Key" name="key" value="<?php echo!empty($_GET['key']) ? $_GET['key'] : ""; ?>" />
        </div>
        <button type="submit" class="btn btn-primary">Search</button>
    </form> <br/>
    <div class="">
        <a href="/admin/categories/add/" class="btn btn-info pull-right">Add Category</a>
    </div>
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Category Name</th>
            <th width="80px">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php if (!empty($categories)) { ?>
            <?php foreach ($categories as $category) { ?>
                <tr>
                    <td><?php echo $category['category_id']; ?></td>
                    <td><?php echo $category['category_name']; ?></td>
                    <td>
                        <a href="/admin/categories/edit/?category_id=<?php echo $category['category_id']; ?>" title='Edit'><i class="glyphicon glyphicon-edit"></i></a>
                        <a href="/admin/categories/?act=status&category_id=<?php echo $category['category_id']; ?>&sta=<?php echo ($category['is_active'] == "1") ? "0" : "1"; ?>" title='<?php echo ($category['is_active'] == "1") ? "Active" : "Inactive"; ?>'><i class="glyphicon glyphicon-star <?php echo ($category['is_active'] == "1") ? "" : "grey"; ?>"></i></a>
                        <a href="/admin/categories/?act=del&category_id=<?php echo $category['category_id']; ?>" title='Delete' onclick="return window.confirm('Do You Want to Delete?');" ><i class="glyphicon glyphicon-trash"></i></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="3">No Categories found.</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php echo!empty($PAGING) ? $PAGING : ""; ?>
</div>