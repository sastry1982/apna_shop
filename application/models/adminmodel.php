<?php

class AdminModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Orders
    function addorder($pdata) {
        $this->db->insert("order", $pdata);
        return $this->db->insert_id();
    }

    function updateOrder($pdata, $order_id) {
        $this->db->where("order_id", $order_id);
        return $this->db->update("order", $pdata);
    }

    function delOrder($order_id) {
        $this->db->where("order_id", $order_id);
        return $this->db->delete("order");
    }

    function getOrderById($order_id) {
        $this->db->select("m.*");
        $this->db->where("order_id", $order_id);
        $query = $this->db->get("order m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function searchOrders($s = array(), $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*, u.name, u.mobile");
        }
        $this->db->join("member u", "m.member_id = u.member_id", "left");
        if (!empty($s['shop_id'])) {
            $this->db->where("m.shop_id", $s['shop_id']);
        }
        $this->db->order_by("m.order_id DESC");
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $query = $this->db->get("order m");
        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    // Members
    function addMember($pdata) {
        $this->db->insert("member", $pdata);
        return $this->db->insert_id();
    }

    function updateMember($pdata, $member_id) {
        $this->db->where("member_id", $member_id);
        return $this->db->update("member", $pdata);
    }

    function delMember($member_id) {
        $this->db->where("member_id", $member_id);
        return $this->db->delete("member");
    }

    function getMemberById($member_id) {
        $this->db->select("m.*, l.parent_id as city_id");
        $this->db->where("m.member_id", $member_id);
        $this->db->join("location l", "m.location_id = l.location_id", "left");
        $query = $this->db->get("member m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function searchMembers($s = array(), $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*");
        }
        if (!empty($s['key'])) {
            $this->db->where("(m.name LIKE '%" . $s['key'] . "%' OR m.email LIKE '%" . $s['key'] . "%' OR m.mobile LIKE '%" . $s['key'] . "%')");
        }
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $this->db->order_by("m.member_id DESC");
        $query = $this->db->get("member m");

        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    function getShopMembersList() {
        $this->db->select("m.*");
        $this->db->where("m.member_type", "SHOP_OWNER");
        $query = $this->db->get("member m");
        if ($query->num_rows() > 0) {
            $rows = array();
            foreach ($query->result_array() as $row) {
                $rows[$row['member_id']] = $row['name'];
            }
            return $rows;
        }
        return false;
    }

    // Shops
    function addShop($pdata) {
        $this->db->insert("shop", $pdata);
        return $this->db->insert_id();
    }

    function updateShop($pdata, $shop_id) {
        $this->db->where("shop_id", $shop_id);
        return $this->db->update("shop", $pdata);
    }

    function delShop($shop_id) {
        $this->db->where("shop_id", $shop_id);
        return $this->db->delete("shop");
    }

    function getShopById($shop_id) {
        $this->db->select("m.*, u.name AS owner_name");
        $this->db->where("shop_id", $shop_id);
        $this->db->join("member u", "m.owner_id=u.member_id");
        $query = $this->db->get("shop m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function getShopByOwnerId($owner_id) {
        $this->db->select("m.*");
        $this->db->where("m.owner_id", $owner_id);
        $query = $this->db->get("shop m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function searchShops($s = array(), $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*, u.name AS member_name");
        }
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $this->db->join("member u", "m.owner_id=u.member_id", "left");
        $this->db->order_by("m.shop_id DESC");
        $query = $this->db->get("shop m");

        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    // Locations
    function addLocation($pdata) {
        $this->db->insert("location", $pdata);
        return $this->db->insert_id();
    }

    function updateLocation($pdata, $location_id) {
        $this->db->where("location_id", $location_id);
        return $this->db->update("location", $pdata);
    }

    function delLocation($location_id) {
        $this->db->where("location_id", $location_id);
        return $this->db->delete("location");
    }

    function getLocationById($location_id) {
        $this->db->select("m.*");
        $this->db->where("location_id", $location_id);
        $query = $this->db->get("location m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function searchLocations($s = array(), $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*, pl.location_name AS parent_location_name");
        }
        if (!empty($s['key'])) {
            $this->db->where("m.location_name LIKE '%" . $s['key'] . "%'");
        }
        $this->db->join("location pl", "m.parent_id = pl.location_id", "left");
        $this->db->order_by("m.location_id DESC");
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $query = $this->db->get("location m");

        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    function getLocationsList($parent_id = "0", $is_active = false) {
        $this->db->select("m.*");
        $this->db->where("parent_id", $parent_id);
        if ($is_active !== false) {
            $this->db->where("is_active", $is_active);
        }
        $query = $this->db->get("location m");
        if ($query->num_rows() > 0) {
            $rows = array();
            foreach ($query->result_array() as $row) {
                $rows[$row['location_id']] = $row['location_name'];
            }
            return $rows;
        }
        return false;
    }

    // Categories
    function addCategory($pdata) {
        $this->db->insert("shop_category", $pdata);
        return $this->db->insert_id();
    }

    function updateCategory($pdata, $category_id) {
        $this->db->where("category_id", $category_id);
        return $this->db->update("shop_category", $pdata);
    }

    function delCategory($category_id) {
        $this->db->where("category_id", $category_id);
        return $this->db->delete("shop_category");
    }

    function getCategoryById($category_id) {
        $this->db->select("m.*");
        $this->db->where("category_id", $category_id);
        $query = $this->db->get("shop_category m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function searchCategories($s = array(), $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*");
        }
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $this->db->order_by("m.category_id DESC");
        $query = $this->db->get("shop_category m");

        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    function getCategoriesList() {
        $this->db->select("m.*");
        $query = $this->db->get("shop_category m");
        if ($query->num_rows() > 0) {
            $rows = array();
            foreach ($query->result_array() as $row) {
                $rows[$row['category_id']] = $row['category_name'];
            }
            return $rows;
        }
        return false;
    }

    function getCountries() {
        $this->db->select("m.*");
        $query = $this->db->get("tbl_countries m");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    function getMemberByEmail($email) {
        $this->db->select("*");
        $this->db->where("email", $email);
        $query = $this->db->get("member");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    // Admin
    function addAdmin($pdata) {
        $this->db->insert("admin", $pdata);
        return $this->db->insert_id();
    }

    function updateAdmin($admin_id, $pdata) {
        $this->db->where("admin_id", $admin_id);
        return $this->db->update("admin", $pdata);
    }

    function delAdmin($admin_id) {
        $this->db->where("admin_id", $admin_id);
        return $this->db->delete("admin");
    }

    function getAdminById($admin_id) {
        $this->db->select("m.*");
        $this->db->where("m.admin_id", $admin_id);
        $query = $this->db->get("admin m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function adminLogin($email, $pwd) {
        $this->db->select("*");
        $this->db->where("email_id", $email);
        $this->db->where("pwd", $pwd);
        $query = $this->db->get("tbl_admin");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

}

?>