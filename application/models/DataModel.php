<?php
/**
 * Description of DataModel
 *
 * @author Sastry
 */
class DataModel extends CI_Model {

    public $table_name = null;
    public $primary_key = null;
    public $actionColumn = true;
    public $paging_data = "";
    public $per_page = 50;
    public $offset = 0;
    public $base_url = '';
    public $grid_settings = array();
    private $header_data = array();
    private $action_btn_data = array();
    public $_rules = array(
        'required' => array(),
        'string' => array(),
        'email' => array(),
        'date' => array(),
        'integer' => array()
    );
    public $search_criteria = array();
    public $multibleSelect = false;
    public $multibleActionHtml = '';
    public $action_btn_settings = array();
    
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
    }

    function search($mode = '') {
        return false;
    }

    function setHeader($header) {
        $this->header_data = $header;
    }

    function setActionBtn($actions, $actionBtnSettings = array()) {
        $this->action_btn_data = $actions;
        $this->action_btn_settings = $actionBtnSettings;
    }

    function setGridSettings($grid_settings) {
        $this->grid_settings = array_merge($this->grid_settings, $grid_settings);
    }

    function getDataGridHtml() {
        $grid_data = "";
        $grid_data .= "<table class='table table-striped' >" . "\n";
        if (!empty($this->header_data) && count($this->header_data) > 0) {
            $grid_data .= '<tr>';
            if ($this->multibleSelect)
                $grid_data .= '<th style="padding: 4px; width: 10px;"> <input type="checkbox" class="selAll" id="selAll" name="selAll" value="" /> </th>';
            foreach ($this->header_data as $column) {
                if (is_array($column)) {
                    $grid_data .= '<th ';
                    if (!empty($column['width']))
                        $grid_data .= 'width="' . $column['width'] . '" ';
                    if (!empty($column['align']))
                        $grid_data .= 'align="' . $column['align'] . '" ';
                    if (!empty($column['style']))
                        $grid_data .= 'style="' . $column['style'] . '" ';
                    $grid_data .= ' >' . $column['label'] . '</th>';
                }else {
                    $grid_data .= '<th>' . $column . '</th>';
                }
            }
            if ($this->actionColumn || count($this->action_btn_data) > 0) {
                $grid_data .= '<th scope="col" ';
                if (!empty($this->action_btn_settings['width']))
                    $grid_data .= 'width="' . $this->action_btn_settings['width'] . '" ';
                if (!empty($this->action_btn_settings['align']))
                    $grid_data .= 'align="' . $this->action_btn_settings['align'] . '" ';
                if (!empty($this->action_btn_settings['style']))
                    $grid_data .= 'style="' . $this->action_btn_settings['style'] . '" ';
                $grid_data .= '>Action</th>';
            }
            $grid_data .= '</tr>';
        }
        
        $this->query = $this->search(true);
        if ($this->query->num_rows() > 0) {
            $p = new pagination();
            $p->items($this->query->num_rows());
            $p->limit($this->per_page); // Limit entries per page
            if(!empty($this->search_criteria) && count($this->search_criteria)>0){
                $query_str = "?".http_build_query($this->search_criteria);
                $p->target($query_str);
            } else {
                $p->target("");
            }            
            $p->currentPage(isset($_GET['paging']) == TRUE ? $_GET['paging'] : 0); // Gets and validates the current page
            $p->calculate(); // Calculates what to show
            $p->parameterName('paging');
            $p->adjacents(1); //No. of page away from the current page
            if (!isset($_GET['paging'])) {
                $p->page = 1;
            } else {
                $p->page = $_GET['paging'];
            }
            $this->offset = (($p->page - 1) * $p->limit);            
            $this->query = $this->search();
        }
        if ($this->query->num_rows() > 0) {
            foreach ($this->query->result_array() as $row) {
                $grid_data .= '<tr>';
                if ($this->multibleSelect)
                    $grid_data .= '<td style="padding: 4px;"><input type="checkbox" class="mSel" name="mSel[]" value="' . $row[$this->primary_key] . '" /></td>';
                foreach ($this->header_data as $key => $column) {
                    if (is_array($column) && !empty($column['method'])) {
                        if (method_exists($this, $column['method']))
                            $row[$key] = call_user_func_array(array($this, $column['method']), array($row));
                        else
                            $row[$key] = call_user_func_array($column['method'], array($row[$key]));                        
                    }
                    if (isset($row[$key]))
                        $grid_data .= '<td>' . $row[$key] . '</td>';
                    else
                        $grid_data .= '<td> </td>';
                }
                if ($this->actionColumn) {
                    $grid_data .= '<td>' . $this->getActionBtnHtml($row) . '</td>';
                }
                $grid_data .= '</tr>';
            }
        } else {
            $colCnt = count($this->header_data);
            if ($this->actionColumn)
                $colCnt += 1;
            if ($this->multibleSelect)
                $colCnt += 1;
            $grid_data .= '<tr>';
            $grid_data .= '<td colspan="' . $colCnt . '" align="center">No records found.</td>';
            $grid_data .= '</tr>';
        }
        $grid_data .= "</table>" . "\n";
        if ($this->multibleSelect && $this->query->num_rows() > 0) {
            if (!empty($this->multibleActionHtml)) {
                $grid_data .= '<div style="margin-left: 15px;">'.$this->multibleActionHtml.' </div>' . "\n";;
            } else {
                $grid_data .= '<div style="margin-left: 15px;"> <select class="mSelAction" name="mSelAction">';
                $grid_data .= '<option value="">--None--</option>';
                $grid_data .= '<option value="mulDel">Delete Seleted</option>';
                $grid_data .= '</select> </div>' . "\n";
            }
        }
        if(!empty($p)){
            $grid_data .= $p->getOutput();
        }
        return $grid_data;
    }

    function getActionBtnHtml($row = array()) {
        $action_btm_html = "";
        if (!empty($this->action_btn_data)) {
            $btn = "";
            foreach ($this->action_btn_data as $btn) {
                if (!empty($btn['href'])) {                    
                    if(!isset($btn['href'])) $btn['href'] = "";
                    if(!isset($btn['onclick'])) $btn['onclick'] = "";
                    $btnHtml = '<a ';
                    foreach ($row as $key => $val) {
                        $btn['href'] = str_replace("{" . $key . "}", $val, $btn['href']);
                        $btn['onclick'] = str_replace("{" . $key . "}", $val, $btn['onclick']);
                        //$btn['after_element'] = str_replace("{" . $key . "}", $val, $btn['after_element']);
                    }
                    $btnHtml .= 'href="' . $btn['href'] . '" ';
                    if (!empty($btn['class']))
                        $btnHtml .= 'class="table-icon ' . $btn['class'] . '" ';
                    if (!empty($btn['width']))
                        $btnHtml .= 'width="' . $btn['width'] . '" ';
                    if (!empty($btn['onclick']))
                        $btnHtml .= "onclick='" . $btn['onclick'] . "' ";
                    $btnHtml .= '> ';
                    if (!empty($btn['label']))
                        $btnHtml .= $btn['label'];
                    $btnHtml .= ' <a/>';
                    if (!empty($btn['after_element'])){
                        $btnHtml .= " ".$btn['after_element'];
                    }
                    $action_btm_html .= $btnHtml;
                }
            }
        }
        return $action_btm_html;
    }

    function getPagenationHtml() {
        return "";
        $paging_data = '<div class="entry">';
        $paging_data .= '<div class="pagination">';
        if (!empty($this->grid_settings['base_url'])){
            $config['base_url'] = $this->grid_settings['base_url'];
        }
        if (!empty($this->base_url)){
            $config['base_url'] = $this->base_url;
        }
        $config['total_rows'] = $this->query->num_rows();
        $config['per_page'] = $this->per_page;
        //$config['uri_segment'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $paging_data .= $this->pagination->create_links();
        $paging_data .= '</div></div>';
        return $paging_data;
    }

    public function attributeLabels() {
        return array();
    }

    public function rules() {
        return array();
    }

    function loadForm($frm_name = '', $fdata = array()) {
        $html_data = "";
        $field_pre = "";
        if (!empty($frm_name))
            $field_pre = $frm_name . "_";
        $labels = $this->attributeLabels();
        $this->_rules = $this->rules() + $this->_rules;
        if (is_array($labels) && count($labels) > 0) {
            foreach ($labels as $key => $label) {
                if (is_array($label))
                    $label_name = $label['label'];
                else
                    $label_name = $label;
                if (!empty($label_name)) {
                    $field_name = $key;
                    $css_class = "";
                    $field_val = !empty($fdata[$key]) ? $fdata[$key] : "";
                    if (in_array($key, $this->_rules['date'])) {
                        $css_class .= " date";
                        if ($field_val == "0000-00-00")
                            $field_val = "";
                        else if (!empty($field_val))
                            $field_val = dateDB2SHOW($field_val);
                    }
                    if (!empty($frm_name))
                        $field_name = $frm_name . '[' . $key . ']';
                    $html_data .= "\n" . '<div class="element">';
                    $html_data .= '<label for="' . $field_pre . $key . '">' . $label_name . ':</label>';
                    $html_data .= '<input class="' . $css_class . '" type="text" name="' . $field_name . '" id="' . $field_pre . $key . '" value="' . $field_val . '" />';
                    $html_data .= '</div>';
                }
            }
        }
        return $html_data;
    }

    function insert($pdata = array()) {
        if (!empty($pdata['created']) && stripos($pdata['created'], "NOW") !== false) {
            $this->db->set('created', $pdata['created'], FALSE);
            unset($pdata['created']);
        }
        $this->db->insert($this->table_name, $pdata);
        return $this->db->insert_id();
    }

    function add($pdata = array()) {
        if (!empty($pdata['created']) && stripos($pdata['created'], "NOW") !== false) {
            $this->db->set('created', $pdata['created'], FALSE);
            unset($pdata['created']);
        }
        $this->db->insert($this->table_name, $pdata);
        return $this->db->insert_id();
    }

    function updateByPk($pk_val, $pdata = array()) {
        if (!empty($pdata['updated']) && stripos($pdata['updated'], "NOW") !== false) {
            $this->db->set('updated', $pdata['updated'], FALSE);
            unset($pdata['updated']);
        }
        $this->db->where($this->primary_key, $pk_val);
        return $this->db->update($this->table_name, $pdata);
    }
    
    function updateById($pk_val, $pdata = array()) {
        $this->updateByPk($pk_val, $pdata);
    }
    
    function update($condition, $pdata = array()) {
        if (!empty($pdata['updated']) && stripos($pdata['updated'], "NOW") !== false) {
            $this->db->set('updated', $pdata['updated'], FALSE);
            unset($pdata['updated']);
        }
        $this->db->where($condition, false);
        return $this->db->update($this->table_name, $pdata);
    }

    function deleteByPk($pk_val) {
        $this->db->where($this->primary_key, $pk_val);
        return $this->db->delete($this->table_name);
    }

    function delete($condition) {
        $this->db->where($condition, false);
        return $this->db->delete($this->table_name);
    }
    
    public function insertFormData($pdata = array()) {
        $data = array();
        $labels = $this->attributeLabels();
        if (is_array($labels) && count($labels) > 0) {
            $this->_rules = $this->rules() + $this->_rules;
            foreach ($labels as $key => $label) {
                if (in_array($key, $this->_rules['date']))
                    $data[$key] = dateForm2DB($pdata[$key]);
                else if (isset($pdata[$key]))
                    $data[$key] = trim($pdata[$key]);
            }
        }
        if (isset($labels['created']))
            $this->db->set('created', "NOW()", FALSE);
        $this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    public function updateFormDataByPk($pk_val, $pdata = array()) {
        $data = array();
        $labels = $this->attributeLabels();
        if (is_array($labels) && count($labels) > 0) {
            $this->_rules = $this->rules() + $this->_rules;
            foreach ($labels as $key => $label) {
                if (in_array($key, $this->_rules['date']))
                    $data[$key] = dateForm2DB($pdata[$key]);
                else if (isset($pdata[$key]))
                    $data[$key] = trim($pdata[$key]);
            }
        }
        $this->db->where($this->primary_key, $pk_val);
        if (isset($labels['updated']))
            $this->db->set('updated', "NOW()", FALSE);
        return $this->db->update($this->table_name, $data);
    }
    
    public function updateFormData($condition, $pdata = array()) {
        $data = array();
        $labels = $this->attributeLabels();
        if (is_array($labels) && count($labels) > 0) {
            $this->_rules = $this->rules() + $this->_rules;
            foreach ($labels as $key => $label) {
                if (in_array($key, $this->_rules['date']))
                    $data[$key] = dateForm2DB($pdata[$key]);
                else if (isset($pdata[$key]))
                    $data[$key] = trim($pdata[$key]);
            }
        }
        $this->db->where($condition);
        if (isset($labels['updated']))
            $this->db->set('updated', "NOW()", FALSE);
        return $this->db->update($this->table_name, $data);
    }

    function getById($pk_val) {
        $this->db->select("*");
        $this->db->where($this->primary_key, $pk_val);
        $query = $this->db->get($this->table_name);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }
    
    function getByPk($pk_val){
        return $this->getById($pk_val);
    }
    
    function find($condition) {
        $this->db->select("*");
        $this->db->where($condition);
        $query = $this->db->get($this->table_name);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function findAll($condition) {
        $this->db->select("*");
        $this->db->where($condition);
        $query = $this->db->get($this->table_name);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

}

?>