<?php

class DBAPI extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    //Address
    function getAddressByMemberId($member_id){
        $this->db->select("m.*");
        if(!empty($s['member_id'])){
            $this->db->where("m.member_id", $member_id);
        }
        $this->db->order_by("m.is_primary DESC");
        $query = $this->db->get("member_address m");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }

    function addShop($pdata) {
        $this->db->insert("shop", $pdata);
        return $this->db->insert_id();
    }

    // Orders
    function addOrder($pdata)
    {
        $this->db->set("order_date_time", "NOW()", false);
        $this->db->insert("order", $pdata);
        return $this->db->insert_id();
    }

    function updateOrder($order_id, $pdata)
    {
        $this->db->where("order_id", $order_id);
        return $this->db->update("order", $pdata);
    }

    function delOrder($order_id)
    {
        $this->db->where("order_id", $order_id);
        return $this->db->delete("order");
    }

    function getOrderById($order_id)
    {
        $this->db->select("m.*");
        $this->db->where("m.order_id", $order_id);
        $query = $this->db->get("order m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function searchOrders($s = array(), $mode = "DATA")
    {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*, s.shop_name");
        }
        if(!empty($s['member_id'])){
            $this->db->where("m.member_id", $s['member_id']);
        }
        $this->db->join("shop s", "m.shop_id = s.shop_id", "left");
        $this->db->order_by("m.order_id DESC");
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $query = $this->db->get("order m");
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    function searchShopOrders($s = array(), $mode = "DATA")
    {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*, s.shop_name, u.name, u.mobile");
        }
        if(!empty($s['member_id'])){
            $this->db->where("s.owner_id", $s['member_id']);
        }
        $this->db->join("shop s", "m.shop_id = s.shop_id");
        $this->db->join("member u", "m.member_id = u.member_id");
        $this->db->order_by("m.order_id DESC");
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $query = $this->db->get("order m");
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    function addOrderItem($pdata)
    {
        $this->db->insert("order_item", $pdata);
        return $this->db->insert_id();
    }

    function updateOrderItem($item_id, $pdata)
    {
        $this->db->where("item_id", $item_id);
        return $this->db->update("order_item", $pdata);
    }

    function delOrderItem($item_id)
    {
        $this->db->where("item_id", $item_id);
        return $this->db->delete("order_item");
    }

    function getOrderItemById($item_id)
    {
        $this->db->select("m.*");
        $this->db->where("m.item_id", $item_id);
        $query = $this->db->get("order_item m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function getOrderItems($order_id)
    {
        $this->db->select("m.*");
        $this->db->where("m.order_id", $order_id);
        $query = $this->db->get("order_item m");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    function searchShops($s = array(), $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("m.*, l.location_name, pl.location_name AS city_name");
        }
        if(!empty($s['key'])){
            $this->db->where("m.shop_name LIKE '%".$s['key']."%'");
        }
        if(!empty($s['city_id'])){
            $this->db->where("m.city_id", $s['city_id']);
        }
        if(!empty($s['location_id'])){
            $this->db->where("m.location_id", $s['location_id']);
        }
        if(!empty($s['category_id'])){
            $this->db->where("m.category_id", $s['category_id']);
        }
        $this->db->where("m.is_active", "1");
        $this->db->join("location l", "m.location_id = l.location_id", "left");
        $this->db->join("location pl", "l.parent_id = pl.location_id", "left");
        $this->db->order_by("m.location_id DESC");
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $query = $this->db->get("shop m");

        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    function addFavoriteShop($shop_id, $member_id){
        $this->db->select("*");
        $this->db->where("shop_id", $shop_id);
        $this->db->where("member_id", $member_id);
        $query = $this->db->get("member_shop");
        if ($query->num_rows() > 0) {
            return true;
        }else{
            $this->db->insert("member_shop", ['member_id' => $member_id, 'shop_id' => $shop_id]);
            return true;
        }
    }

    function searchFavoriteShops($s = array(), $mode = "DATA") {
        if ($mode == "CNT") {
            $this->db->select("COUNT(1) as CNT");
        } else {
            $this->db->select("s.*");
            $this->db->select("l.location_name, pl.location_name AS city_name");
        }
        if(!empty($s['city_id'])){
            $this->db->where("s.city_id", $s['city_id']);
        }
        if(!empty($s['location_id'])){
            $this->db->where("s.location_id", $s['location_id']);
        }
        if(!empty($s['category_id'])){
            $this->db->where("s.category_id", $s['category_id']);
        }
        $this->db->join("shop s", "m.shop_id = s.shop_id");
        $this->db->join("location l", "s.location_id = l.location_id", "left");
        $this->db->join("location pl", "l.parent_id = pl.location_id", "left");
        $this->db->order_by("s.shop_id ASC");
        if (isset($s['limit']) && isset($s['offset'])) {
            $this->db->limit($s['limit'], $s['offset']);
        }
        $query = $this->db->get("member_shop m");

        if ($query->num_rows() > 0) {
            if ($mode == "CNT") {
                $row = $query->row_array();
                return $row['CNT'];
            }
            return $query->result_array();
        }
        return false;
    }

    function getShopById($shop_id)
    {
        $this->db->select("m.*");
        $this->db->where("m.shop_id", $shop_id);
        //$this->db->join("location l", "m.location_id=l.location_id", "left");
        $query = $this->db->get("shop m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function getShopByMemberId($member_id)
    {
        $this->db->select("m.*");
        $this->db->where("m.owner_id", $member_id);
        //$this->db->join("location l", "m.location_id=l.location_id", "left");
        $query = $this->db->get("shop m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function getLocationsList($parent_id = "0", $is_active = false)
    {
        $this->db->select("m.*");
        $this->db->where("parent_id", $parent_id);
        if ($is_active !== false) {
            $this->db->where("is_active", $is_active);
        }
        $query = $this->db->get("location m");
        if ($query->num_rows() > 0) {
            $rows = array();
            foreach ($query->result_array() as $row) {
                $rows[$row['location_id']] = $row['location_name'];
            }
            return $rows;
        }
        return array();
    }

    function getCategoriesList()
    {
        $this->db->select("m.*");
        $this->db->where("is_active", "1");
        $query = $this->db->get("shop_category m");
        if ($query->num_rows() > 0) {
            $rows = array();
            foreach ($query->result_array() as $row) {
                $rows[$row['category_id']] = $row['category_name'];
            }
            return $rows;
        }
        return array();
    }

    // Members
    function addMember($pdata)
    {
        if (!empty($pdata['pwd'])) {
            $pdata['pwd'] = md5($pdata['pwd']);
        }
        $this->db->insert("member", $pdata);
        return $this->db->insert_id();
    }

    function updateMember($member_id, $pdata)
    {
        if (!empty($pdata['pwd'])) {
            $pdata['pwd'] = md5($pdata['pwd']);
        }
        $this->db->where("member_id", $member_id);
        return $this->db->update("member", $pdata);
    }

    function delMember($member_id)
    {
        $this->db->where("member_id", $member_id);
        return $this->db->delete("member");
    }

    function getMemberById($member_id)
    {
        $this->db->select("m.*, l.location_name, l.parent_id AS city_id, c.location_name AS city_name");
        $this->db->where("m.member_id", $member_id);
        $this->db->join("location l", "m.location_id=l.location_id", "left");
        $this->db->join("location c", "l.parent_id=c.location_id", "left");
        $query = $this->db->get("member m");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    function checkMemberEmail($email, $member_id = '')
    {
        $this->db->select("*");
        if (!empty($member_id)) {
            $this->db->where("member_id !=", $member_id);
        }
        $this->db->where("email", $email);
        $query = $this->db->get("member");
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

    function checkMemberMobile($mobile, $member_id = '')
    {
        $this->db->select("*");
        if (!empty($member_id)) {
            $this->db->where("member_id !=", $member_id);
        }
        $this->db->where("mobile", $mobile);
        $query = $this->db->get("member");
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

    function memberLogin($email, $pwd)
    {
        $this->db->select("*");
        $this->db->where("email", $email);
        $this->db->where("pwd", md5($pwd));
        $query = $this->db->get("member");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

    // Admin 
    function adminLogin($email, $pwd)
    {
        $this->db->select("*");
        $this->db->where("email", $email);
        $this->db->where("pwd", $pwd);
        $query = $this->db->get("admin");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

}

?>