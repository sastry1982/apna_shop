    <style>
        .signup {
            padding-top: 50px;
            color: red;
            text-align: center;
        }
        @media (min-width: 1200px) {
            .fgh {
                width: 70%;
            }
        }
        @media only screen and (max-width: 480px) {
            .signup {
                padding-top: 50px;
                color: red;
                text-align: center;
            }
        }
        @media only screen and (max-width: 700px), only screen and (max-device-width: 700px) {
            #button-submit {
                margin-left: 40px;
                padding-left: 10px;
                padding-right: 10px;
            }
        }
    </style>
<div class="row">
    <div class="col-lg-12 text-center">
        <div class="col-lg-3"></div>
            <form class="form-horizontal center col-sm-7" name="frm" id="frm" method="post">
                <h1 class="signup">Shop Owner Sign Up </h1>
                <div class="form-group">
                    <label for="name" class="control-label col-sm-3">Name</label>
                    <div class="col-sm-6">
                        <input type="text" id="name" name="name" class="form-control required" placeholder="Name"
                               value="<?php echo !empty($_POST['name']) ? $_POST['name'] : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="gender" class="control-label col-sm-3">Gender</label>
                    <div class="col-sm-6">
                        <select name="gender" id="gender" class="form-control">
                            <option
                                value="Male" <?php (!empty($_POST['gender']) && $_POST['gender'] == "Male") ? 'selected="true"' : ""; ?> >
                                Male
                            </option>
                            <option
                                value="Female" <?php (!empty($_POST['gender']) && $_POST['gender'] == "Female") ? 'selected="true"' : ""; ?>  >
                                Female
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="mobile" class="control-label col-sm-3">Mobile</label>
                    <div class="col-sm-6">
                        <input type="text" id="mobile" name="mobile" class="form-control required" placeholder="Mobile"
                               value="<?php echo !empty($_POST['mobile']) ? $_POST['mobile'] : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-sm-3 control-label">City</label>
                    <div class="col-sm-6">
                        <select name="city" id="city" class="form-control required">
                            <?php if (!empty($parent_locations)) { ?>
                                <?php
                                foreach ($parent_locations as $loc_id => $loc) {
                                    if (!empty($_POST['city']) && $_POST['city'] == $loc_id) {
                                        echo '<option value="' . $loc_id . '" selected="true">' . $loc . '</option>';
                                    } else {
                                        echo '<option value="' . $loc_id . '">' . $loc . '</option>';
                                    }
                                }
                                ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="location_id" class="col-sm-3 control-label">Area</label>
                    <div class="col-sm-6">
                        <select name="location_id" id="location_id" class="form-control required">
                            <?php if (!empty($locations)) { ?>
                                <?php
                                foreach ($locations as $loc_id => $loc) {
                                    if (!empty($_POST['location_id']) && $_POST['location_id'] == $loc_id) {
                                        echo '<option value="' . $loc_id . '" selected="true">' . $loc . '</option>';
                                    } else {
                                        echo '<option value="' . $loc_id . '">' . $loc . '</option>';
                                    }
                                }
                                ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="control-label col-sm-3">Email</label>
                    <div class="col-sm-6">
                        <input type="email" id="email" name="email" class="form-control email required"
                               placeholder="Email"
                               value="<?php echo !empty($_POST['email']) ? $_POST['email'] : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="pwd" class="control-label col-sm-3">Password</label>
                    <div class="col-sm-6">
                        <input type="password" id="pwd" name="pwd" class="form-control required" placeholder=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="shop_name" class="control-label col-sm-3">Shop Name</label>
                    <div class="col-sm-6">
                        <input type="text" id="shop_name" name="shop_name" class="form-control required" placeholder="Shop Name" value="<?php echo !empty($_POST['shop_name']) ? $_POST['shop_name'] : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="shop_phone" class="control-label col-sm-3">Shop Phone</label>
                    <div class="col-sm-6">
                        <input type="text" id="shop_phone" name="shop_phone" class="form-control required" placeholder="Shop Phone" value="<?php echo !empty($_POST['shop_phone']) ? $_POST['shop_phone'] : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="category_id" class="control-label col-sm-3">Category</label>
                    <div class="col-sm-6">
                        <select name="category_id" id="category_id" class="form-control required">
                            <option value="">select</option>
                            <?php if (!empty($categories)) { ?>
                                <?php
                                foreach ($categories as $cat_id => $cat) {
                                    if (!empty($_POST['category_id']) && $_POST['category_id'] == $cat_id) {
                                        echo '<option value="' . $cat_id . '" selected="true">' . $cat . '</option>';
                                    } else {
                                        echo '<option value="' . $cat_id . '">' . $cat . '</option>';
                                    }
                                }
                                ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-default" id="button-submit">Submit</button>
                    </div>
                </div>
            </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#frm").validate();
        $("#city").on("change", function () {
            $("#location_id").html("");
            $.ajax({
                type: 'POST',
                url: "/home/areas/" + $("#city").val(),
                dataType: "json",
                cache: false,
                success: function (data) {
                    $.each(data, function (key, value) {
                        $('#location_id').append($("<option></option>").attr("value", key).text(value));
                    });
                }
            });
        });
    });
</script>