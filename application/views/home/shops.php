<div class="container">
    <div class="col-lg-12 ">
        <div class="row" style="padding-top:53px;overflow-x:hidden;padding-left:10px;">
            <h2>Shops</h2>
            <form name="frm" id="frm" class="form-inline">
                <div class="form-group">
                    <label for="cat" class="col-sm-2 control-label label-align" style="margin-right:34px;">Category</label>
                    <div class="col-sm-3">
                        <select name="cat" id="cat" class="form-control">
                            <option value="">Select Category</option>
                            <?php if (!empty($categories)) { ?>
                                <?php
                                foreach ($categories as $cat_id => $cat) {
                                    if (!empty($_GET['cat']) && $_GET['cat'] == $cat_id) {
                                        echo '<option value="' . $cat_id . '" selected="true">' . $cat . '</option>';
                                    } else {
                                        echo '<option value="' . $cat_id . '">' . $cat . '</option>';
                                    }
                                }
                                ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-sm-2 control-label label-align">City</label>

                    <div class="col-sm-3">
                        <select name="city" id="city" class="form-control required">

                            <?php if (!empty($parent_locations)) { ?>
                                <?php
                                foreach ($parent_locations as $loc_id => $loc) {
                                    if (!empty($_GET['city']) && $_GET['city'] == $loc_id) {
                                        echo '<option value="' . $loc_id . '" selected="true">' . $loc . '</option>';
                                    } else {
                                        echo '<option value="' . $loc_id . '">' . $loc . '</option>';
                                    }
                                }
                                ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="loc" class="col-sm-2 control-label label-align">Area</label>

                    <div class="col-sm-3">
                        <select name="loc" id="location_id" class="form-control">
                            <option value="">Any</option>
                            <?php if (!empty($locations)) { ?>
                                <?php
                                foreach ($locations as $loc_id => $loc) {
                                    if (!empty($_GET['location_id']) && $_GET['location_id'] == $loc_id) {
                                        echo '<option value="' . $loc_id . '" selected="true">' . $loc . '</option>';
                                    } else {
                                        echo '<option value="' . $loc_id . '">' . $loc . '</option>';
                                    }
                                }
                                ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <!--<input type="submit" value="Filter" style="text-align:center;@media only screen and (max-width: 480px) { margin-left:110px;padding-left: 20px;padding-right: 20px;}"/>-->
                <button class="btn btn-default" type="submit" id="button-submit"
                        style="padding-left: 20px;padding-right: 20px;">Filter
                </button>
            </form>
        </div>
    </div>

    <?php if (!empty($shops)) { ?>
        <?php foreach ($shops as $shop) { ?>
            <div class="col-lg-12 tablet col-md-4 col-sm-6 col-xs-12">
                <div class="shop-box padding-left" style="margin-top:50px;">
                        <div class="col-lg-4 padding col-xs-12 col-md-12 col-sm-12">
                            <?php if (!empty($shop['shop_logo'])) { ?>
                                <img src="/data/logos/<?php echo $shop['shop_logo']; ?>"
                                     class="img-responsive shop-logo"/>
                            <?php } else { ?>
                                <img src="/data/logos/" class="img-responsive"/>
                            <?php } ?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-4 col-xs-12">
                            <h4><?php echo $shop['shop_name']; ?></h4>

                            <p><i class="fa fa-map-marker"></i> <?php echo $shop['location_name']; ?>
                                , <?php echo $shop['city_name']; ?> </p>

                            <p><i class="fa fa-phone"></i> <?php echo $shop['shop_phone']; ?></p>
                            <section class="media shop-details">
                                <?php echo shortDesc($shop['shop_description'], 200); ?>
                            </section>
                        </div>
                        <div class="col-lg-4 col-xs-12 col-sm-12 col-md-12 cart-block">
                            <a href="/my/add_favorite/<?php echo $shop['shop_id']; ?>/" title='Favorite' class="">
                                <i class="glyphicon glyphicon-heart"></i>
                            </a>
                            <a href="/my/purchase/<?php echo $shop['shop_id']; ?>/" class="btn btn-default">Order Now</a>
                        </div>
                </div>
            </div>
        <?php } ?>
    <?php } else { ?>
        <div class="row content-block">
            No Records Found.
        </div>
    <?php } ?>

</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#frm").validate();
        $("#city").on("change", function () {
            $("#location_id").html("");
            $.ajax({
                type: 'POST',
                url: "/home/areas/" + $("#city").val(),
                dataType: "json",
                cache: false,
                success: function (data) {
                    $('#location_id').append($("<option></option>").attr("value", "").text("Any"));
                    $.each(data, function (key, value) {
                        $('#location_id').append($("<option></option>").attr("value", key).text(value));
                    });
                }
            });
        });
    });
</script>

