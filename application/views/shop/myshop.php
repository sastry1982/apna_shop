<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<style>
    #mapCanvas {
        width: 100%;
        height: 520px;
        float: left;
    }
    #infoPanel {
        float: left;
        margin-left: 10px;
    }
</style>
<div class="container">
    <div class="page-header">
        <h1>
            <div class="small-head">My Shop</div>
        </h1>
    </div>
    <form class="form-horizontal" name="frm" id="frm" method="post">
        <input type="hidden" name="shop_id" id="shop_id" value="<?php echo!empty($shop['shop_id']) ? $shop['shop_id'] : ""; ?>"/>
        <div class="container">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="shop_name" class="col-sm-3 control-label">Shop Name</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control required" id="shop_name" name="shop_name" placeholder="Shop Name" value="<?php echo!empty($shop['shop_name']) ? $shop['shop_name'] : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="shop_logo" class="col-sm-3 control-label">Shop Logo</label>
                    <div class="col-sm-8">
                        <?php
                        if (!empty($shop['shop_logo']) && file_exists(DOC_ROOT_PATH . "/data/logos/" . $shop['shop_logo'])) {
                            echo '<img src="/data/logos/' . $shop['shop_logo'] . '" height="100px" /><br/>';
                        }
                        ?>
                        <input type="file" class="form-control" id="shop_logo" name="shop_logo" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="shop_phone" class="col-sm-3 control-label">Shop Phone</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="shop_phone" name="shop_phone" placeholder="Shop Phone" value="<?php echo!empty($shop['shop_phone']) ? $shop['shop_phone'] : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="category_id" class="col-sm-3 control-label">Category</label>
                    <div class="col-sm-8">
                        <select name="category_id" id="category_id" class="form-control required">
                            <option value="">select</option>
                            <?php if (!empty($categories)) { ?>
                                <?php
                                foreach ($categories as $cat_id => $cat) {
                                    if (!empty($shop['category_id']) && $shop['category_id'] == $cat_id) {
                                        echo '<option value="' . $cat_id . '" selected="true">' . $cat . '</option>';
                                    } else {
                                        echo '<option value="' . $cat_id . '">' . $cat . '</option>';
                                    }
                                }
                                ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-sm-3 control-label">City</label>
                    <div class="col-sm-8">
                        <select name="city_id" id="city" class="form-control required">
                            <?php if (!empty($parent_locations)) { ?>
                                <?php
                                foreach ($parent_locations as $loc_id => $loc) {
                                    if (!empty($shop['city_id']) && $shop['city_id'] == $loc_id) {
                                        echo '<option value="' . $loc_id . '" selected="true">' . $loc . '</option>';
                                    } else {
                                        echo '<option value="' . $loc_id . '">' . $loc . '</option>';
                                    }
                                }
                                ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="location_id" class="col-sm-3 control-label">Area</label>
                    <div class="col-sm-8">
                        <select name="location_id" id="location_id" class="form-control required">
                            <?php if (!empty($locations)) { ?>
                                <?php
                                foreach ($locations as $loc_id => $loc) {
                                    if (!empty($shop['location_id']) && $shop['location_id'] == $loc_id) {
                                        echo '<option value="' . $loc_id . '" selected="true">' . $loc . '</option>';
                                    } else {
                                        echo '<option value="' . $loc_id . '">' . $loc . '</option>';
                                    }
                                }
                                ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="shop_address" class="col-sm-3 control-label">Shop Address</label>
                    <div class="col-sm-8">
                        <textarea class="form-control required" id="shop_address" name="shop_address"><?php echo!empty($shop['shop_address']) ? $shop['shop_address'] : ""; ?></textarea>
                        <input type="button" class="address_search_btn btn btn-default" name="" value="Search" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="latitude" class="col-sm-3 control-label">Latitude</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="latitude" name="latitude" value="<?php echo!empty($shop['latitude']) ? $shop['latitude'] : ""; ?>" readonly="true"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="longitude" class="col-sm-3 control-label">Longitude</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="longitude" name="longitude" value="<?php echo!empty($shop['longitude']) ? $shop['longitude'] : ""; ?>" readonly="true"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div id="mapCanvas"></div>
                <div id="infoPanel"></div>
            </div>
        </div>
        <div class="form-group">
            <label for="shop_description" class="col-sm-2 control-label">Shop Description</label>
            <div class="col-sm-10">
                <textarea class="form-control required" id="shop_description" name="shop_description" rows="5"><?php echo!empty($shop['shop_description']) ? $shop['shop_description'] : ""; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default"><?php echo!empty($shop['shop_id']) ? "Update" : "Submit"; ?></button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    var geocoder = new google.maps.Geocoder();
    $(document).ready(function () {
        $("#frm").validate();
        $("#city").on("change", function () {
            $("#location_id").html("");
            $.ajax({
                type: 'POST',
                url: "/home/areas/" + $("#city").val(),
                dataType: "json",
                cache: false,
                success: function (data) {

                    $.each(data, function (key, value) {
                        $('#location_id').append($("<option></option>").attr("value", key).text(value));
                    });
                }
            });
        });
        $(".address_search_btn").on("click", function(){
            geocoder.geocode({ 'address': $("#shop_address").val() }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();

                    var latLng = new google.maps.LatLng(latitude, longitude);
                    var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                        zoom: 14,
                        center: latLng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
                    var marker = new google.maps.Marker({
                        position: latLng,
                        //title: 'Point A',
                        map: map,
                        draggable: true
                    });
                    $("#latitude").val(latitude);
                    $("#longitude").val(longitude);

                    // Add dragging event listeners.
                    //google.maps.event.addListener(marker, 'dragstart', function() { });

                    google.maps.event.addListener(marker, 'drag', function() {
                        $("#latitude").val(marker.getPosition().lat());
                        $("#longitude").val(marker.getPosition().lng());
                    });

                    google.maps.event.addListener(marker, 'dragend', function() {
                        $("#latitude").val(marker.getPosition().lat());
                        $("#longitude").val(marker.getPosition().lng());
                        geocodePosition(marker.getPosition());
                    });

                } else {
                    alert("Sorry we unable to find your address.")
                }
            });
        });
    });
    <?php if(!empty($shop['latitude']) && !empty($shop['longitude'])){ ?>
    function initialize() {
        var latLng = new google.maps.LatLng(<?php echo $shop['latitude']; ?>, <?php echo $shop['longitude']; ?>);
        var map = new google.maps.Map(document.getElementById('mapCanvas'), {
            zoom: 14,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            draggable: true
        });

        // Add dragging event listeners.
        google.maps.event.addListener(marker, 'drag', function() {
            $("#latitude").val(marker.getPosition().lat());
            $("#longitude").val(marker.getPosition().lng());
        });

        google.maps.event.addListener(marker, 'dragend', function() {
            $("#latitude").val(marker.getPosition().lat());
            $("#longitude").val(marker.getPosition().lng());
            geocodePosition(marker.getPosition());
        });
    }
    // Onload handler to fire off the app.
    google.maps.event.addDomListener(window, 'load', initialize);
    <?php } ?>
    function geocodePosition(pos) {
        geocoder.geocode({
            latLng: pos
        }, function(responses) {
            if (responses && responses.length > 0) {
                $("#infoPanel").html(responses[0].formatted_address);
            } else {
                $("#infoPanel").html("");
            }
        });
    }
</script>