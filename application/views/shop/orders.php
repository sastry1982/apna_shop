<div class="container">
    <div class="page-header">
        <h1><div class="small-head">Orders</div></h1>
    </div>
    <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Key" name="key" value="<?php echo!empty($_GET['key']) ? $_GET['key'] : ""; ?>" />
        </div>
        <button type="submit" class="btn btn-primary">Search</button>
    </form> <br/>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Mobile</th>
                <th>Order Amount</th>
                <th>Order Status</th>
                <th>Order Address</th>
                <th>Order Date</th>
                <th width="80px">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php if (!empty($orders)) { ?>
            <?php foreach ($orders as $order) { ?>
                <tr id="row<?php echo $order['order_id']; ?>">
                    <td><?php echo $order['order_id']; ?></td>
                    <td><?php echo $order['name']; ?></td>
                    <td><?php echo $order['mobile']; ?></td>
                    <td><?php echo $order['order_total']; ?></td>
                    <td><?php echo $order['order_status']; ?></td>
                    <td><?php echo $order['order_status']; ?></td>
                    <td><?php echo !empty($order['order_date_time'])?dateTimeDB2SHOW($order['order_date_time']):""; ?></td>
                    <td>
                        <a class="order_accept <?php echo ($order['order_status'] == "Accepted")?"":"gray"; ?>" href="/shop/order_accept/<?php echo $order['order_id']; ?>/" title='Accept'><i class="glyphicon glyphicon-ok-circle"></i></a>
                        <a class="order_reject <?php echo ($order['order_status'] == "Rejected")?"":"gray"; ?>" href="/shop/order_reject/<?php echo $order['order_id']; ?>/" title='Reject'><i class="glyphicon glyphicon-remove-circle"></i></a>
                        <a class="order_view" href="/shop/order_items/?order_id=<?php echo $order['order_id']; ?>" title="Show Order"><i class="glyphicon glyphicon-shopping-cart"></i></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="7">No Orders found.</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php echo!empty($PAGING) ? $PAGING : ""; ?>
</div>
<div class="modal fade" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="itemContent">
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('a.order_view').click(function(event){
            event.preventDefault();
            $.ajax({
                url: $(this).attr("href"),
                type: 'POST',
                success: function(data) {
                    $("#itemContent").html(data);
                    $("#orderModal").modal("show");
                }
            });
        });
        $('a.order_accept').click(function(event){
            event.preventDefault();
            if(confirm("Do you want to Accept this order?")) {
                var thisEle = $(this);
                $.ajax({
                    url: $(this).attr("href"),
                    type: 'POST',
                    success: function (data) {
                        if(data == "Yes"){
                            thisEle.removeClass("gray");
                            thisEle.removeClass("order_accept");
                        }else{
                        }
                    }
                });
            }
        });
        $('a.order_reject').click(function(event){
            event.preventDefault();
            if(confirm("Do you want to Reject this order?")) {
                var thisEle = $(this);
                $.ajax({
                    url: $(this).attr("href"),
                    type: 'POST',
                    success: function (data) {
                        if(data == "Yes"){
                            thisEle.removeClass("gray");
                            thisEle.removeClass("order_reject");
                        }else{
                        }
                    }
                });
            }
        });
    });
</script>