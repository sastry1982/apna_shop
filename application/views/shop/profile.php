<div class="container">
    <div class="page-header">
        <h1>
            <div class="small-head">Profile</div>
        </h1>
    </div>
    <form class="form-horizontal" name="frm" id="frm" method="post">
        <div class="form-group">
            <label for="name" class="control-label col-lg-2">Name</label>

            <div class="col-lg-4">
                <input type="text" id="name" name="name" class="form-control required" placeholder="Name"
                       value="<?php echo !empty($member['name']) ? $member['name'] : ""; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="gender" class="control-label col-lg-2">Gender</label>

            <div class="col-lg-2">
                <select name="gender" id="gender" class="form-control">
                    <option
                        value="Male" <?php (!empty($member['gender']) && $member['gender'] == "Male") ? 'selected="true"' : ""; ?> >
                        Male
                    </option>
                    <option
                        value="Female" <?php (!empty($member['gender']) && $member['gender'] == "Female") ? 'selected="true"' : ""; ?>  >
                        Female
                    </option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="mobile" class="control-label col-lg-2">Mobile</label>

            <div class="col-lg-4">
                <input type="text" id="mobile" name="mobile" class="form-control required" placeholder="Mobile"
                       value="<?php echo !empty($member['mobile']) ? $member['mobile'] : ""; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="city" class="col-sm-2 control-label">City</label>

            <div class="col-sm-3">
                <select name="city" id="city" class="form-control required">
                    <?php if (!empty($parent_locations)) { ?>
                        <?php
                        foreach ($parent_locations as $loc_id => $loc) {
                            if (!empty($member['city']) && $member['city'] == $loc_id) {
                                echo '<option value="' . $loc_id . '" selected="true">' . $loc . '</option>';
                            } else {
                                echo '<option value="' . $loc_id . '">' . $loc . '</option>';
                            }
                        }
                        ?>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="location_id" class="col-sm-2 control-label">Area</label>

            <div class="col-sm-3">
                <select name="location_id" id="location_id" class="form-control required">
                    <?php if (!empty($locations)) { ?>
                        <?php
                        foreach ($locations as $loc_id => $loc) {
                            if (!empty($member['location_id']) && $member['location_id'] == $loc_id) {
                                echo '<option value="' . $loc_id . '" selected="true">' . $loc . '</option>';
                            } else {
                                echo '<option value="' . $loc_id . '">' . $loc . '</option>';
                            }
                        }
                        ?>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#frm").validate();
        $("#city").on("change", function () {
            $("#location_id").html("");
            $.ajax({
                type: 'POST',
                url: "/home/areas/" + $("#city").val(),
                dataType: "json",
                cache: false,
                success: function (data) {

                    $.each(data, function (key, value) {
                        $('#location_id').append($("<option></option>").attr("value", key).text(value));
                    });
                }
            });
        });
    });
</script>