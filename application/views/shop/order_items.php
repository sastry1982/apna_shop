<form name="item_frm" id="item_frm" method="post">
    <input type="hidden" name="order_id" id="order_id" value="<?php echo $order['order_id']; ?>" />
    <input type="hidden" name="ORDER_ITEM_UPDATE" value="true" />
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Order #<?php echo $order['order_id']; ?> <small><?php echo !empty($order['order_date_time'])?dateTimeDB2SHOW($order['order_date_time'], "d M, Y H:i:s"):""; ?></small> </h4>        
    </div>
    <div class="modal-body">        
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Item Name</th>
                    <th style="width: 60px;">quantity</th>
                    <th>Units</th>
                    <th style="width: 100px;">Price</th>
                </tr>
            </thead>
            <tbody>
            <?php if (!empty($order_items)) { $sno = 1; ?>
                <?php if(in_array($order['order_status'], ["Accepted", "Process"])){ ?>
                    <?php foreach ($order_items as $item) { ?>
                        <tr>
                            <td><?php echo $sno++; ?></td>
                            <td><?php echo $item['item_name']; ?></td>
                            <td><input type="text" class="form-control input-sm" name="item_qty[<?php echo $item['item_id']; ?>]" id="item_qty<?php echo $item['item_id']; ?>" value="<?php echo $item['item_qty']; ?>" /></td>
                            <td><?php echo $item['item_unit']; ?></td>
                            <td><input type="text" class="form-control input-sm" name="item_price[<?php echo $item['item_id']; ?>]" id="item_price<?php echo $item['item_id']; ?>" value="<?php echo $item['item_price']; ?>" /></td>
                        </tr>
                    <?php } ?>
                    <?php }else{ ?>
                    <?php foreach ($order_items as $item) { ?>
                        <tr>
                            <td><?php echo $sno++; ?></td>
                            <td><?php echo $item['item_name']; ?></td>
                            <td><?php echo $item['item_qty']; ?></td>
                            <td><?php echo $item['item_unit']; ?></td>
                            <td><?php echo $item['item_price']; ?></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td colspan="5">No Orders found.</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <section class="form-horizontal structured">
            <div class="form-group">
                <label class="col-sm-3 control-label">Order Amount :</label>
                <div class="col-sm-3 "><?php echo $order['order_total']; ?></div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Order Status :</label>
                <div class="col-sm-3">
                    <select name="order_status" id="order_status" class="form-control">
                        <option value="Accepted">Accepted</option>
                        <option value="Rejected">Rejected</option>
                        <option value="Processed">Processed</option>
                        <option value="Shipping">Shipping</option>
                        <option value="Shipped">Shipped</option>
                        <option value="Returned">Returned</option>
                        <option value="Canceled">Canceled</option>
                        <option value="Completed">Completed</option>
                    </select>
                </div>
            </div>
        </section>
        <section class="form-horizontal structured">
            <div class="form-group">
                <label class="col-sm-3 control-label"> Name :</label>
                <div class="col-sm-8"><?php echo !empty($member['name'])?$member['name']:""; ?></div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"> Mobile :</label>
                <div class="col-sm-8"><?php echo !empty($member['mobile'])?$member['mobile']:""; ?></div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Address:</label>
                <div class="col-sm-8">
                    <?php echo !empty($member['address'])?$member['address']."<br/>":""; ?>
                    <?php echo !empty($member['location_name'])?$member['location_name']:""; ?>
                    <?php echo !empty($member['city_name'])?", ".$member['city_name']:""; ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Order Status:</label>
                <div class="col-sm-8"><?php echo !empty($order['order_status'])?$order['order_status']:""; ?></div>
            </div>
        </section>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <?php if(in_array($order['order_status'], ["Accepted", "Processed", "Shipping"])){ ?>
        <button type="submit" class="btn btn-default">Save</button>
        <?php } ?>
    </div>
</form>