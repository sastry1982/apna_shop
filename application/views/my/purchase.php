<div class="container">
    <h2>Order List for <?php echo !empty($shop['shop_name']) ? $shop['shop_name'] : ''; ?></h2>
    <div class="">
        <form name="frm" id="frm" class="form-horizontal" method="post">
            <input type="hidden" name="shop_id" id="shop_id" value="<?php echo !empty($shop['shop_id']) ? $shop['shop_id'] : ''; ?>"/>
            <div class="form-group">
                <label for="items" class="col-md-2 control-label">Items</label>
                <div class="col-md-8" id="mblock">
                    <a href="#" class="btn addbtn icon" id="addbtn"> <i class="glyphicon glyphicon-plus"></i> </a>
                    <?php if (!empty($order_items)) { ?>
                        <?php foreach ($order_items as $item) { ?>
                            <div class="row">
                                <div class="col-sm-5">
                                    <input type="text" class="form-control required" name="item_name[]" value="<?php echo !empty($item['item_name']) ? $item['item_name'] : ""; ?>" placeholder="Item Name"/>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control required" name="item_qty[]" value="<?php echo !empty($item['item_qty']) ? $item['item_qty'] : ""; ?>" placeholder="0.00"/>
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control" name="item_unit[]">
                                        <option value="">None</option>
                                        <?php
                                        foreach ($unit_names as $unit) {
                                            if (!empty($item['item_unit']) && $item['item_unit']) {
                                                echo '<option value="' . $unit . '" selected="true">' . $unit . '</option>';
                                            } else {
                                                echo '<option value="' . $unit . '">' . $unit . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <a class="btn remove " id="addbtn"><i class="glyphicon glyphicon-trash"></i></a>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <?php for ($sno = 1; $sno <= 5; $sno++) { ?>
                            <div class="row">
                                <div class="col-sm-5">
                                    <input type="text" class="form-control required" name="item_name[]" value="" placeholder="Item Name"/>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control required" name="item_qty[]" value="" placeholder="0.00"/>
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control" name="item_unit[]">
                                        <option value="">None</option>
                                        <?php
                                        foreach ($unit_names as $unit) {
                                            echo '<option value="' . $unit . '">' . $unit . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <a class="btn remove del" id="addbtn"><i class="glyphicon glyphicon-trash"></i></a>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-9 text-center">
                    <input type="submit" class="btn btn-primary text-center" value="Submit"/>
                </div>
            </div>
        </form>
    </div>
</div>
<div style="display: none;">
    <div id="itemrow">
        <div class="col-sm-5">
            <input type="text" class="form-control required" name="item_name[]" value="" placeholder="Item Name"/>
        </div>
        <div class="col-sm-2">
            <input type="text" class="form-control required" name="item_qty[]" value="" placeholder="0.00"/>
        </div>
        <div class="col-sm-2">
            <select class="form-control" name="item_unit[]">
                <option value="">None</option>
                <?php
                foreach ($unit_names as $unit) {
                    echo '<option value="' . $unit . '">' . $unit . '</option>';
                }
                ?>
            </select>
        </div>
        <a class="btn remove" id="addbtn"><i class="glyphicon glyphicon-trash"></i></a>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#frm").validate();
        $("#addbtn").on("click", function (event) {
            event.preventDefault();
            var mClone = $("#itemrow").html();
            $("#mblock").append('<div class="row">' + mClone + ' </div>');

        });
        $("#mblock").on("click", ".remove", function (event) {
            event.preventDefault();
            $(this).parent(".row").remove();
        });
    });
</script>