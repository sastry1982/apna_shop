<div class="container">
    <div class="page-header">
        <h1> <div class="small-head">My Favourites</div> </h1>
    </div>
    <?php if (!empty($shops)) { ?>
        <?php foreach ($shops as $shop) { ?>
            <div class="col-lg-12 tablet col-md-4 col-sm-6 col-xs-12">
                <div class="shop-box padding-left" style="margin-top:50px;">
                    <div class="col-lg-4 padding col-xs-12 col-md-12 col-sm-12">
                        <?php if (!empty($shop['shop_logo'])) { ?>
                            <img src="/data/logos/<?php echo $shop['shop_logo']; ?>"
                                 class="img-responsive shop-logo"/>
                        <?php } else { ?>
                            <img src="/data/logos/" class="img-responsive"/>
                        <?php } ?>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-4 col-xs-12">
                        <h4><?php echo $shop['shop_name']; ?></h4>

                        <p><i class="fa fa-map-marker"></i> <?php echo $shop['location_name']; ?>
                            , <?php echo $shop['city_name']; ?> </p>

                        <p><i class="fa fa-phone"></i> <?php echo $shop['shop_phone']; ?></p>
                        <section class="media shop-details">
                            <?php echo shortDesc($shop['shop_description'], 200); ?>
                        </section>
                    </div>
                    <div class="col-lg-4 col-xs-12 col-sm-12 col-md-12 cart-block">
                        <a href="/my/purchase/<?php echo $shop['shop_id']; ?>/" class="btn btn-default">Order Now</a>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } else { ?>
        <div class="row content-block text-center">
            No Records Found. Please go to <a href="/shops/">shops page</a> for view list of shops.
        </div>
    <?php } ?>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#frm").validate();
        $("#city").on("change", function () {
            $("#location_id").html("");
            $.ajax({
                type: 'POST',
                url: "/home/areas/" + $("#city").val(),
                dataType: "json",
                cache: false,
                success: function (data) {
                    $('#location_id').append($("<option></option>").attr("value", "").text("Any"));
                    $.each(data, function (key, value) {
                        $('#location_id').append($("<option></option>").attr("value", key).text(value));
                    });
                }
            });
        });
    });
</script>

