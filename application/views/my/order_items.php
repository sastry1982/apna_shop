<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Order #<?php echo $order['order_id']; ?></h4>
</div>
<div class="modal-body">
    <section class="form-horizontal structured">
        <div class="form-group">
            <label class="col-sm-3 control-label">Shop Name :</label>
            <div class="col-sm-8"><?php echo !empty($shop['shop_name'])?$shop['shop_name']:""; ?></div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Order Date:</label>
            <div class="col-sm-8"><?php echo !empty($order['order_date_time'])?dateTimeDB2SHOW($order['order_date_time']):""; ?></div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Order Status:</label>
            <div class="col-sm-8"><?php echo !empty($order['order_status'])?$order['order_status']:""; ?></div>
        </div>
    </section>
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Item Name</th>
            <th>quantity</th>
            <th>Units</th>
            <th>Price</th>
        </tr>
        </thead>
        <tbody>
        <?php if (!empty($order_items)) { $sno = 1; ?>
            <?php foreach ($order_items as $item) { ?>
                <tr>
                    <td><?php echo $sno++; ?></td>
                    <td><?php echo $item['item_name']; ?></td>
                    <td><?php echo $item['item_qty']; ?></td>
                    <td><?php echo $item['item_unit']; ?></td>
                    <td><?php echo $item['item_price']; ?></td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="5">No Orders found.</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>