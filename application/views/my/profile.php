<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<style>
    #mapCanvas {
        width: 500px;
        height: 300px;
        float: left;
    }
    #infoPanel {
        float: left;
        margin-left: 10px;
    }
    #infoPanel div {
        margin-bottom: 5px;
    }
</style>
<div class="container">
    <div class="page-header">
        <h1>
            <div class="small-head">Profile</div>
        </h1>
    </div>
    <form class="form-horizontal" name="frm" id="frm" method="post">
        <div class="container">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="name" class="control-label col-lg-3">Name</label>

                    <div class="col-lg-8">
                        <input type="text" id="name" name="name" class="form-control required" placeholder="Name"
                               value="<?php echo !empty($member['name']) ? $member['name'] : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="gender" class="control-label col-lg-3">Gender</label>

                    <div class="col-lg-3">
                        <select name="gender" id="gender" class="form-control">
                            <option
                                value="Male" <?php (!empty($member['gender']) && $member['gender'] == "Male") ? 'selected="true"' : ""; ?> >
                                Male
                            </option>
                            <option
                                value="Female" <?php (!empty($member['gender']) && $member['gender'] == "Female") ? 'selected="true"' : ""; ?>  >
                                Female
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="mobile" class="control-label col-lg-3">Mobile</label>

                    <div class="col-lg-8">
                        <input type="text" id="mobile" name="mobile" class="form-control required" placeholder="Mobile"
                               value="<?php echo !empty($member['mobile']) ? $member['mobile'] : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-sm-3 control-label">City</label>

                    <div class="col-lg-4">
                        <select name="city" id="city" class="form-control required">
                            <?php if (!empty($parent_locations)) { ?>
                                <?php
                                foreach ($parent_locations as $loc_id => $loc) {
                                    if (!empty($member['city']) && $member['city'] == $loc_id) {
                                        echo '<option value="' . $loc_id . '" selected="true">' . $loc . '</option>';
                                    } else {
                                        echo '<option value="' . $loc_id . '">' . $loc . '</option>';
                                    }
                                }
                                ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="location_id" class="col-sm-3 control-label">Area</label>
                    <div class="col-lg-4">
                        <select name="location_id" id="location_id" class="form-control required">
                            <?php if (!empty($locations)) { ?>
                                <?php
                                foreach ($locations as $loc_id => $loc) {
                                    if (!empty($member['location_id']) && $member['location_id'] == $loc_id) {
                                        echo '<option value="' . $loc_id . '" selected="true">' . $loc . '</option>';
                                    } else {
                                        echo '<option value="' . $loc_id . '">' . $loc . '</option>';
                                    }
                                }
                                ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-6"></div>
        </div>
        <div class="container">
            <div class="address_block col-md-6">
                <div class="form-group">
                    <label for="address" class="col-lg-3 control-label">Address</label>
                    <div class="col-lg-8">
                        <textarea name="address" id="address" class="form-control"><?php echo !empty($member['address'])?$member['address']:""; ?></textarea>
                        <input type="button" class="address_search_btn btn btn-primary" value="Search" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="latitude" class="control-label col-lg-3">Latitude</label>
                    <div class="col-lg-6">
                        <input type="text" id="latitude" name="latitude" class="form-control required" value="<?php echo !empty($member['latitude']) ? $member['latitude'] : ""; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="longitude" class="control-label col-lg-3">Longitude</label>
                    <div class="col-lg-6">
                        <input type="text" id="longitude" name="longitude" class="form-control required" value="<?php echo !empty($member['longitude']) ? $member['longitude'] : ""; ?>"/>
                    </div>
                </div>
            </div>
            <div class="map_block col-md-6">
                <div id="mapCanvas"></div>
                <div id="infoPanel"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    var geocoder = new google.maps.Geocoder();
    $(document).ready(function () {
        $("#frm").validate();
        $("#city").on("change", function () {
            $("#location_id").html("");
            $.ajax({
                type: 'POST',
                url: "/home/areas/" + $("#city").val(),
                dataType: "json",
                cache: false,
                success: function (data) {
                    $.each(data, function (key, value) {
                        $('#location_id').append($("<option></option>").attr("value", key).text(value));
                    });
                }
            });
        });
        $(".address_search_btn").on("click", function(){
            geocoder.geocode({ 'address': $("#address").val() }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();

                    var latLng = new google.maps.LatLng(latitude, longitude);
                    var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                        zoom: 14,
                        center: latLng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
                    var marker = new google.maps.Marker({
                        position: latLng,
                        //title: 'Point A',
                        map: map,
                        draggable: true
                    });
                    $("#latitude").val(latitude);
                    $("#longitude").val(longitude);

                    // Add dragging event listeners.
                    //google.maps.event.addListener(marker, 'dragstart', function() { });

                    google.maps.event.addListener(marker, 'drag', function() {
                        $("#latitude").val(marker.getPosition().lat());
                        $("#longitude").val(marker.getPosition().lng());
                    });

                    google.maps.event.addListener(marker, 'dragend', function() {
                        $("#latitude").val(marker.getPosition().lat());
                        $("#longitude").val(marker.getPosition().lng());
                        geocodePosition(marker.getPosition());
                    });

                } else {
                    alert("Sorry we unable to find your address.")
                }
            });
        });
    });
<?php if(!empty($member['latitude']) && !empty($member['longitude'])){ ?>
    function initialize() {
        var latLng = new google.maps.LatLng(<?php echo $member['latitude']; ?>, <?php echo $member['longitude']; ?>);
        var map = new google.maps.Map(document.getElementById('mapCanvas'), {
            zoom: 14,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            draggable: true
        });

        // Add dragging event listeners.
        google.maps.event.addListener(marker, 'drag', function() {
            $("#latitude").val(marker.getPosition().lat());
            $("#longitude").val(marker.getPosition().lng());
        });

        google.maps.event.addListener(marker, 'dragend', function() {
            $("#latitude").val(marker.getPosition().lat());
            $("#longitude").val(marker.getPosition().lng());
            geocodePosition(marker.getPosition());
        });
    }
    // Onload handler to fire off the app.
    google.maps.event.addDomListener(window, 'load', initialize);
<?php } ?>
    function geocodePosition(pos) {
        geocoder.geocode({
            latLng: pos
        }, function(responses) {
            if (responses && responses.length > 0) {
                $("#infoPanel").html(responses[0].formatted_address);
            } else {
                $("#infoPanel").html("");
            }
        });
    }
</script>
<!--script type="text/javascript">
    var geocoder = new google.maps.Geocoder();

    function geocodePosition(pos) {
        geocoder.geocode({
            latLng: pos
        }, function(responses) {
            if (responses && responses.length > 0) {
                updateMarkerAddress(responses[0].formatted_address);
            } else {
                updateMarkerAddress('Cannot determine address at this location.');
            }
        });
    }

    function updateMarkerStatus(str) {
        document.getElementById('markerStatus').innerHTML = str;
    }

    function updateMarkerPosition(latLng) {
        $("#latitude").val(latLng.lat());
        $("#longitude").val(latLng.lng());
        document.getElementById('info').innerHTML = [
            latLng.lat(),
            latLng.lng()
        ].join(', ');
    }

    function updateMarkerAddress(str) {
        document.getElementById('address').innerHTML = str;
    }

    function initialize() {
        var latLng = new google.maps.LatLng(-34.397, 150.644);
        var map = new google.maps.Map(document.getElementById('mapCanvas'), {
            zoom: 8,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var marker = new google.maps.Marker({
            position: latLng,
            title: 'Point A',
            map: map,
            draggable: true
        });

        // Update current position info.
        updateMarkerPosition(latLng);
        geocodePosition(latLng);

        // Add dragging event listeners.
        google.maps.event.addListener(marker, 'dragstart', function() {
            updateMarkerAddress('Dragging...');
        });

        google.maps.event.addListener(marker, 'drag', function() {
            updateMarkerStatus('Dragging...');
            updateMarkerPosition(marker.getPosition());
        });

        google.maps.event.addListener(marker, 'dragend', function() {
            updateMarkerStatus('Drag ended');
            geocodePosition(marker.getPosition());
        });
    }

    // Onload handler to fire off the app.
    google.maps.event.addDomListener(window, 'load', initialize);

    var address = document.getElementById("address").value;
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            alert("Latitude: " + latitude + "\nLongitude: " + longitude);
        } else {
            alert("Request failed.")
        }
    });
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(setPosition);
    }
    function setPosition(position) {
        $("#latitude").val(position.coords.latitude);
        $("#longitude").val(position.coords.longitude);
    }
</script-->
