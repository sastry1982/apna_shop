<div class="container">
    <div class="page-header">
        <h1>
            <div class="small-head">Orders</div>
        </h1>
    </div>
    <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Key" name="key"
                   value="<?php echo !empty($_GET['key']) ? $_GET['key'] : ""; ?>"/>
        </div>
        <button type="submit" class="btn btn-primary">Search</button>
    </form>
    <br/>
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Shop Name</th>
            <th>Order Amount</th>
            <th>Order Status</th>
            <th>Order Date</th>
            <th width="80px">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php if (!empty($orders)) { ?>
            <?php foreach ($orders as $order) { ?>
                <tr>
                    <td><?php echo $order['order_id']; ?></td>
                    <td><?php echo $order['shop_name']; ?></td>
                    <td><?php echo $order['order_total']; ?></td>
                    <td><?php echo $order['order_status']; ?></td>
                    <td><?php echo !empty($order['order_date_time']) ? dateTimeDB2SHOW($order['order_date_time']) : ""; ?></td>
                    <td>
                        <a class="order_view" href="/my/order_items/?order_id=<?php echo $order['order_id']; ?>" title="Show Order"><i class="glyphicon glyphicon-shopping-cart"></i></a>
                        <a href="/my/purchase/<?php echo $order['shop_id']; ?>/?order_id=<?php echo $order['order_id']; ?>" title='Re-Order'><i class="glyphicon glyphicon-retweet"></i></a>
                    </td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="3">No Orders found.</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?php echo !empty($PAGING) ? $PAGING : ""; ?>
</div>
<div class="modal fade" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="itemContent">
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('a.order_view').click(function(event){
            event.preventDefault();
            $.ajax({
                url: $(this).attr("href"),
                type: 'POST',
                success: function(data) {
                    $("#itemContent").html(data);
                    $("#orderModal").modal("show");
                }
            });
        });
    });
</script>