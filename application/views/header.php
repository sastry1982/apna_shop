<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="Apna Shop">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Apna Shop</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" href="/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/css/style.css" />

    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery.validate.min.js"></script>
</head>
<body>
<header class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Apna Shop</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#">About</a>
                </li>
                
                <li> <a href="/shops/">Shops</a> </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if (!empty($_SESSION['MEMBER_ID'])) { ?>

                    <?php if ($_SESSION['MEMBER_TYPE'] == "CUSTOMER") { ?>
                        <li><a href="/my/orders/">My Orders</a></li>
                        <li><a href="/my/shops/">My Favourites</a></li>
                        <li><a href="/my/profile/">Profile</a></li>
                    <?php }else if ($_SESSION['MEMBER_TYPE'] == "SHOP_OWNER") { ?>
                        <li><a href="/shop/orders/">Orders</a></li>
                        <li><a href="/shop/profile/">My Profile</a></li>
                        <li><a href="/shop/myshop/">My Shop</a></li>
                    <?php } ?>
                    <li><a href="/home/logout/">Logout</a></li>
                <?php } else { ?>
                    <li><a href="/home/shopsignup/">Shop Owner Sign up</a></li>
                    <li><a href="/home/signup/">Customer Sign up</a></li>
                    <li><a href="/login/">Login</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</header>
<div class="home pagecontainer">
    <?php getMessage(); ?>

