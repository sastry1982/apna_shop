-- phpMyAdmin SQL Dump
-- version 4.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Oct 05, 2015 at 12:22 AM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `apna_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `pwd` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `email`, `pwd`, `name`, `is_active`) VALUES
(1, 'admin@apnashops.com', 'Admin@123', 'Admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `location_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `location_name` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`location_id`, `parent_id`, `location_name`, `is_active`) VALUES
(1, 0, 'Hyderabad', 1),
(2, 0, 'Warangal', 1),
(3, 0, 'Vijayavada', 1),
(4, 0, 'Visakhapatnam', 1),
(5, 1, 'Moosapet', 1),
(6, 3, 'Benz Circle', 1),
(7, 1, 'kukatpally', 1),
(8, 0, 'Tenali', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `member_id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `pwd` varchar(200) NOT NULL,
  `user_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `member_id` int(11) NOT NULL,
  `member_type` enum('CUSTOMER','SHOP_OWNER') NOT NULL DEFAULT 'CUSTOMER',
  `name` varchar(100) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `email` varchar(200) NOT NULL,
  `pwd` varchar(200) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `location_id` int(11) NOT NULL DEFAULT '0',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`member_id`, `member_type`, `name`, `gender`, `email`, `pwd`, `mobile`, `location_id`, `is_active`, `added_on`) VALUES
(1, 'CUSTOMER', 'L B Sastry', 'Male', 'sastrylal@gmail.com', '0e7517141fb53f21ee439b355b5a1d0a', '9290266674', 0, 1, '2015-08-26 06:38:33'),
(2, 'SHOP_OWNER', 'Shop001', 'Male', 'sastrylal@yahoo.com', '0e7517141fb53f21ee439b355b5a1d0a', '9290266675', 0, 1, '2015-08-28 13:57:08'),
(3, 'SHOP_OWNER', 'shop3000', 'Male', 'sastrylal+20@yahoo.com', '0e7517141fb53f21ee439b355b5a1d0a', '9290266620', 6, 1, '2015-09-01 08:52:59'),
(4, 'SHOP_OWNER', 'shop400', 'Male', 'sastrylal+400@gmail.com', '0e7517141fb53f21ee439b355b5a1d0a', '9290266676', 7, 1, '2015-09-21 07:20:41');

-- --------------------------------------------------------

--
-- Table structure for table `member_shop`
--

CREATE TABLE `member_shop` (
  `member_id` int(11) NOT NULL DEFAULT '0',
  `shop_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member_shop`
--

INSERT INTO `member_shop` (`member_id`, `shop_id`) VALUES
(1, 4),
(1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `order_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL DEFAULT '0',
  `member_id` int(11) NOT NULL DEFAULT '0',
  `order_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `order_total` decimal(12,2) NOT NULL DEFAULT '0.00',
  `order_status` varchar(20) NOT NULL,
  `payment_status` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`order_id`, `shop_id`, `member_id`, `order_date_time`, `order_total`, `order_status`, `payment_status`) VALUES
(1, 5, 1, '2015-09-22 20:01:56', '20.00', 'Accepted', ''),
(2, 4, 1, '2015-09-22 20:04:17', '80.00', 'Accepted', ''),
(3, 5, 1, '2015-09-22 20:21:09', '51.00', 'Accepted', ''),
(4, 5, 1, '2015-09-23 15:31:06', '300.00', 'Accepted', '');

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `item_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `item_name` varchar(200) NOT NULL,
  `item_qty` decimal(10,2) NOT NULL DEFAULT '0.00',
  `item_unit` varchar(10) NOT NULL,
  `item_unit_price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `item_price` decimal(12,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`item_id`, `order_id`, `item_name`, `item_qty`, `item_unit`, `item_unit_price`, `item_price`) VALUES
(1, 1, 'test 001', '1.00', 'Kgs', '0.00', '10.00'),
(2, 1, 'test 002', '2.00', 'Liters', '0.00', '10.00'),
(3, 2, 'my test items sopa', '1.00', '', '0.00', '10.00'),
(4, 2, 'test 001', '1.00', '', '0.00', '10.00'),
(5, 2, 'test 002', '1.00', '', '0.00', '20.00'),
(6, 2, 'test 003', '1.00', 'Kgs', '0.00', '20.00'),
(7, 2, 'test 004', '2.00', 'Kgs', '0.00', '20.00'),
(8, 3, 'test 001', '1.00', '', '0.00', '1.00'),
(9, 3, 'test 002', '1.00', '', '0.00', '20.00'),
(10, 3, 'test 003', '1.00', '', '0.00', '30.00'),
(11, 3, 'test 004', '1.00', '', '0.00', '0.00'),
(12, 3, 'test 005', '1.00', '', '0.00', '0.00'),
(13, 3, 'test 006', '1.00', '', '0.00', '0.00'),
(14, 3, 'test 007', '1.00', '', '0.00', '0.00'),
(15, 3, 'test 008', '1.00', '', '0.00', '0.00'),
(16, 3, 'test 009', '1.00', '', '0.00', '0.00'),
(17, 3, 'test 0010', '0.00', '', '0.00', '0.00'),
(18, 3, 'test 0011', '0.00', '', '0.00', '0.00'),
(19, 3, 'test 0012', '0.00', '', '0.00', '0.00'),
(20, 3, 'test 0013', '0.00', '', '0.00', '0.00'),
(21, 3, 'test 0014', '0.00', '', '0.00', '0.00'),
(22, 4, 'test0002', '1.00', 'Kgs', '0.00', '100.00'),
(23, 4, 'test 003', '2.00', 'Kgs', '0.00', '200.00');

-- --------------------------------------------------------

--
-- Table structure for table `order_log`
--

CREATE TABLE `order_log` (
  `log_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `action` varchar(100) NOT NULL,
  `message` varchar(255) NOT NULL,
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `status_id` tinyint(4) NOT NULL,
  `short_name` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

CREATE TABLE `shop` (
  `shop_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `shop_name` varchar(200) NOT NULL,
  `shop_logo` varchar(200) NOT NULL,
  `shop_phone` varchar(20) NOT NULL,
  `city_id` int(11) NOT NULL DEFAULT '0',
  `location_id` int(11) NOT NULL DEFAULT '0',
  `shop_address` varchar(255) NOT NULL,
  `shop_description` text NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop`
--

INSERT INTO `shop` (`shop_id`, `category_id`, `owner_id`, `shop_name`, `shop_logo`, `shop_phone`, `city_id`, `location_id`, `shop_address`, `shop_description`, `is_active`) VALUES
(1, 3, 2, 'Shpo001', 'logo1.jpeg', '9290266675', 1, 5, 'test12', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 1),
(4, 0, 3, 'shop3000', 'logo4.jpeg', '929299292', 3, 6, 'This is 300 shop2', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 1),
(5, 3, 4, 'shop400', 'logo5.jpeg', '929299292', 1, 7, 'test', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shop_category`
--

CREATE TABLE `shop_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(200) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop_category`
--

INSERT INTO `shop_category` (`category_id`, `category_name`, `is_active`) VALUES
(2, 'test001', 1),
(3, 'test002', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD UNIQUE KEY `member_id` (`member_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `member_shop`
--
ALTER TABLE `member_shop`
  ADD UNIQUE KEY `member_id` (`member_id`,`shop_id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `order_log`
--
ALTER TABLE `order_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`shop_id`);

--
-- Indexes for table `shop_category`
--
ALTER TABLE `shop_category`
  ADD PRIMARY KEY (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `order_log`
--
ALTER TABLE `order_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_status`
--
ALTER TABLE `order_status`
  MODIFY `status_id` tinyint(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop`
--
ALTER TABLE `shop`
  MODIFY `shop_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `shop_category`
--
ALTER TABLE `shop_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;