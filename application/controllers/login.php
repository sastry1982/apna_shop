<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->load->model("DBAPI", "dbapi", TRUE);
    }

    public function index() {
        if (!empty($_POST['email'])) {
            $member = $this->dbapi->memberLogin($_POST['email'], $_POST['pwd']);
            $redirect_url = !empty($_SESSION['redirect_url'])?$_SESSION['redirect_url']:"";
            $_SESSION = array();
            if (!empty($member['member_id'])) {
                $_SESSION['NAME'] = $member['name'];
                $_SESSION['MOBILE'] = $member['mobile'];
                $_SESSION['EMAIL'] = $member['email'];
                $_SESSION['MEMBER_ID'] = $member['member_id'];
                $_SESSION['MEMBER_TYPE'] = $member['member_type'];
                $_SESSION['ROLE'] = $member['member_type'];
                if($member['member_type'] == "SHOP_OWNER"){
                    redirect(base_url() . "shop/");
                }else{
                    if(!empty($redirect_url)){
                        redirect(base_url().ltrim($redirect_url, "/"));
                    }
                    redirect(base_url() . "my/");
                }
            }else{
                $_SESSION['error'] = "Invalid email/password";
            }
        }
        $data = array();
        $this->_template('login', $data);
    }

}
