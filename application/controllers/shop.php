<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shop extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->_member_login_check('SHOP_OWNER');
        $this->load->model("AdminModel", "admin", TRUE);
        $this->load->model("DBAPI", "dbapi", TRUE);
    }

    public function index() {
        redirect(base_url()."shop/orders/");
    }

    public function orders(){
        $data = array();
        if(!empty($_POST['order_id']) && !empty($_POST['ORDER_ITEM_UPDATE'])){
            if(!empty($_POST['item_price'])){
                $total_amount = 0.00;
                foreach($_POST['item_price'] as $item_id => $item_price){
                    $item = [];
                    $item['item_price'] = $item_price;
                    if(isset($_POST['item_qty'][$item_id])){
                        $item['item_qty'] = $_POST['item_qty'][$item_id];
                    }
                    $total_amount += $item_price;
                    $this->dbapi->updateOrderItem($item_id, $item);
                }
                $this->dbapi->updateOrder($_POST['order_id'], ['order_total' => $total_amount, 'order_status' => $_POST['order_status']]);
            }
            if(!empty($_SERVER['REDIRECT_URL'])){
                redirect(base_url().ltrim($_SERVER['REDIRECT_URL'], "/"));
            }
            redirect(base_url()."shop/orders/");
        }
        if (!empty($_GET['act']) && $_GET['act'] == "del" && !empty($_GET['category_id'])) {
            $this->admin->delCategory($_GET['category_id']);
            redirect(base_url() . "admin/orders/");
        }
        if (!empty($_GET['act']) && $_GET['act'] == "status" && !empty($_GET['category_id']) && isset($_GET['sta'])) {
            $is_active = (!empty($_GET['sta']) && $_GET['sta'] == "1") ? "1" : "0";
            $this->admin->updateCategory(array("is_active" => $is_active), $_GET['category_id']);
            redirect(base_url() . "admin/orders/");
        }
        $search_data = array();
        if (!empty($this->_REQ['key'])) {
            $search_data['key'] = $this->_REQ['key'];
        }
        $search_data['member_id'] = $_SESSION['MEMBER_ID'];
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = $search_data;
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/shop/orders/?';
        $this->pagenavi->process($this->dbapi, 'searchShopOrders');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['orders'] = $this->pagenavi->items;
        $this->_template('shop/orders', $data);
    }

    public function order_items(){
        $data = array();
        $data['order'] = $this->dbapi->getOrderById($this->_REQ['order_id']);
        $data['shop'] = $this->dbapi->getShopById($data['order']['shop_id']);
        $data['member'] = $this->dbapi->getMemberById($data['order']['member_id']);
        $data['order_items'] = $this->dbapi->getOrderItems($this->_REQ['order_id']);
        $this->load->view('shop/order_items', $data);
    }

    public function order_accept($order_id){
        if($this->admin->updateOrder(['order_status' => 'Accepted'], $order_id)){
            echo "Yes";
        }else{
            echo "No";
        }
    }

    public function order_reject($order_id){
        if($this->admin->updateOrder(['order_status' => 'Rejected'], $order_id)){
            echo "Yes";
        }else{
            echo "No";
        }
    }

    public function order_pending($order_id){
        if($this->admin->updateOrder(['order_status' => 'Pending'], $order_id)){
            echo "Yes";
        }else{
            echo "No";
        }
    }

    public function orders_json(){
        $search_data = array();
        $search_data['shop_id'] = "1";
        $oders = array();
        $rows = $this->admin->searchOrders($search_data);
        foreach($rows as $row){
            $order = [];
            $order[] = $row['order_id'];
            $order[] = $row['name'];
            $order[] = $row['mobile'];
            $order[] = $row['order_total'];
            $order[] = $row['order_status'];
            $order[] = $row['order_date_time'];
            $oders[] = $order;
        }
        $oders = ['data' => $oders];
        echo json_encode($oders);
        exit;
    }

    public function profile()
    {
        $data = array();
        if (!empty($_POST['name'])) {
            $pdata = array();
            $pdata['name'] = !empty($_POST['name']) ? trim($_POST['name']) : "";
            $pdata['gender'] = !empty($_POST['gender']) ? $_POST['gender'] : "";
            $pdata['mobile'] = !empty($_POST['mobile']) ? trim($_POST['mobile']) : "";
            $pdata['is_active'] = "1";
            $mobile_exist = $this->dbapi->checkMemberMobile($pdata['mobile'], $_SESSION['MEMBER_ID']);
            if ($mobile_exist) {
                $_SESSION['error'] = "Already registered with this Mobile";
            } else {
                $this->dbapi->updateMember($_SESSION['MEMBER_ID'], $pdata);
                $_SESSION['message'] = "Your Profile has been updated successfully.";
                redirect(base_url() . "shop/profile/");
            }
        }
        $data['locations'] = [];
        $data['parent_locations'] = $this->dbapi->getLocationsList("0", "1");
        if (!empty($data['parent_locations'])) {
            foreach ($data['parent_locations'] as $loc_id => $loc) {
                $data['locations'] = $this->dbapi->getLocationsList($loc_id, "1");
                break;
            }
        }
        $data['member'] = $this->dbapi->getMemberById($_SESSION['MEMBER_ID']);
        $this->_template("shop/profile", $data);
    }

    public function myshop()
    {
        $data = array();
        if (!empty($_POST['shop_id'])) {
            $pdata = array();
            $pdata['shop_name'] = !empty($_POST['shop_name']) ? trim($_POST['shop_name']) : "";
            $pdata['shop_phone'] = !empty($_POST['shop_phone']) ? trim($_POST['shop_phone']) : "";
            $pdata['category_id'] = !empty($_POST['category_id']) ? trim($_POST['category_id']) : "";
            $pdata['city_id'] = !empty($_POST['city_id']) ? trim($_POST['city_id']) : "";
            $pdata['location_id'] = !empty($_POST['location_id']) ? trim($_POST['location_id']) : "";
            $pdata['shop_address'] = !empty($_POST['shop_address']) ? trim($_POST['shop_address']) : "";
            $pdata['latitude'] = !empty($_POST['latitude']) ? trim($_POST['latitude']) : "";
            $pdata['longitude'] = !empty($_POST['longitude']) ? trim($_POST['longitude']) : "";
            $pdata['shop_description'] = !empty($_POST['shop_description']) ? trim($_POST['shop_description']) : "";
            $img = imgUpload('shop_logo', '/data/logos/', 'logo' . $_POST['shop_id']);
            if ($img !== false) {
                $pdata['shop_logo'] = $img;
            }
            $this->admin->updateShop($pdata, $_POST['shop_id']);
            $_SESSION['message'] = "Your Shop has been updated successfully.";
            redirect(base_url() . "shop/myshop/");
        }
        $data['locations'] = [];
        $data['parent_locations'] = $this->dbapi->getLocationsList("0", "1");
        if (!empty($data['parent_locations'])) {
            foreach ($data['parent_locations'] as $loc_id => $loc) {
                $data['locations'] = $this->dbapi->getLocationsList($loc_id, "1");
                break;
            }
        }
        $data['shop'] = $this->dbapi->getShopByMemberId($_SESSION['MEMBER_ID']);
        $data['categories'] = $this->dbapi->getCategoriesList();
        $this->_template("shop/myshop", $data);
    }

    public function dashboard() {
        $data = array();
        $this->_template('shop/dashboard', $data);
    }

    public function logout(){
        redirect(base_url()."login/");
    }

}