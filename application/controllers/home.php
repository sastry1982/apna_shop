<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MY_Controller {

    public $header_data = array();

    function __construct() {
        parent::__construct();
        $this->load->model("DBAPI", "dbapi", TRUE);
    }

    public function index() {
        $data = array();
        $this->_template("home/home", $data);
    }

    public function shops(){
        $data = array();
        $search_data = array();
        $city_id = "";
        if (!empty($this->_REQ['cat'])) {
            $search_data['category_id'] = $this->_REQ['cat'];
        }
        if (!empty($this->_REQ['city'])) {
            $search_data['city_id'] = $this->_REQ['city'];
            $city_id = $this->_REQ['city'];
        }
        if (!empty($this->_REQ['loc'])) {
            $search_data['location_id'] = $this->_REQ['loc'];
        }
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = $search_data;
        $this->pagenavi->per_page = 30;
        $this->pagenavi->base_url = '/shops/';
        $this->pagenavi->process($this->dbapi, 'searchShops');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['shops'] = $this->pagenavi->items;
        $data['locations'] = [];
        $data['parent_locations'] = $this->dbapi->getLocationsList("0", "1");
        if(!empty($data['parent_locations'])){
            if(empty($city_id)) {
                foreach ($data['parent_locations'] as $loc_id => $loc) {
                    $city_id = $loc_id;
                    break;
                }
            }
            $data['locations'] = $this->dbapi->getLocationsList($city_id, "1");
        }
        $data['categories'] = $this->dbapi->getCategoriesList();
        $this->_template('home/shops', $data);
    }

    public function signup() {
        $data = array();
        if (!empty($_POST['email'])) {
            $pdata = array();
            $pdata['name'] = !empty($_POST['name']) ? trim($_POST['name']) : "";
            $pdata['email'] = !empty($_POST['email']) ? trim($_POST['email']) : "";
            $pdata['pwd'] = !empty($_POST['pwd']) ? $_POST['pwd'] : 'Admin@123';
            $pdata['gender'] = !empty($_POST['gender']) ? $_POST['gender'] : "";
            $pdata['mobile'] = !empty($_POST['mobile']) ? trim($_POST['mobile']) : "";
            $pdata['is_active'] = "1";
            $email_exist = $this->dbapi->checkMemberEmail($pdata['email']);
            $mobile_exist = $this->dbapi->checkMemberMobile($pdata['mobile']);
            if($mobile_exist){
                $_SESSION['error'] = "Already registered with this Mobile";
            }else if ($email_exist) {
                $_SESSION['error'] = "Already registered with this Email address";
            } else {
                $this->dbapi->addMember($pdata);
                $_SESSION['message'] = "Your Sign up has been completed successfully.";
                redirect(base_url());
            }
        }
        $data['locations'] = [];
        $data['parent_locations'] = $this->dbapi->getLocationsList("0", "1");
        if(!empty($data['parent_locations'])){
            foreach($data['parent_locations'] as $loc_id=>$loc){
                $data['locations'] = $this->dbapi->getLocationsList($loc_id, "1");
                break;
            }
        }
        $this->_template("home/signup", $data);
    }

    public function shopsignup() {
        $data = array();
        if (!empty($_POST['email'])) {
            $pdata = array();
            $pdata['name'] = !empty($_POST['name']) ? trim($_POST['name']) : "";
            $pdata['email'] = !empty($_POST['email']) ? trim($_POST['email']) : "";
            $pdata['pwd'] = !empty($_POST['pwd']) ? $_POST['pwd'] : 'Admin@123';
            $pdata['gender'] = !empty($_POST['gender']) ? $_POST['gender'] : "";
            $pdata['mobile'] = !empty($_POST['mobile']) ? trim($_POST['mobile']) : "";
            $pdata['is_active'] = "1";
            $pdata['member_type'] = "SHOP_OWNER";
            $email_exist = $this->dbapi->checkMemberEmail($pdata['email']);
            $mobile_exist = $this->dbapi->checkMemberMobile($pdata['mobile']);
            if($mobile_exist){
                $_SESSION['error'] = "Already registered with this Mobile";
            }else if ($email_exist) {
                $_SESSION['error'] = "Already registered with this Email address";
            } else {
                $member_id = $this->dbapi->addMember($pdata);
                if(!empty($member_id)){
                    $shop = array();
                    $shop['owner_id'] = $member_id;
                    $shop['shop_name'] = !empty($_POST['shop_name']) ? trim($_POST['shop_name']) : "";
                    $shop['shop_phone'] = !empty($_POST['shop_phone']) ? $_POST['shop_phone'] : "";
                    $shop['category_id'] = !empty($_POST['category_id']) ? $_POST['category_id'] : "";
                    $shop['is_active'] = "0";
                    $this->dbapi->addShop($shop);
                }
                $_SESSION['message'] = "Your Sign up has been completed successfully.";
                redirect(base_url());
            }
        }
        $data['locations'] = [];
        $data['parent_locations'] = $this->dbapi->getLocationsList("0", "1");
        if(!empty($data['parent_locations'])){
            foreach($data['parent_locations'] as $loc_id=>$loc){
                $data['locations'] = $this->dbapi->getLocationsList($loc_id, "1");
                break;
            }
        }
        $data['categories'] = $this->dbapi->getCategoriesList();
        $this->_template("home/shopsignup", $data);
    }

    public function areas($loc=''){
        $locations = [];
        if(!empty($loc)){
            $locations = $this->dbapi->getLocationsList($loc, "1");
        }
        echo json_encode($locations);
        exit;
    }
    
    public function logout() {
        $_SESSION = array();
        redirect(base_url()."login/");
    }

}
