<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class My extends MY_Controller
{

    public $header_data = array();

    function __construct()
    {
        parent::__construct();
        $this->load->model("DBAPI", "dbapi", TRUE);
        $this->_member_login_check();
    }

    public function index()
    {
        $data = array();
        $this->_template("my/dashboard", $data);
    }

    public function purchase($shop_id = '')
    {
        $data = array();
        if(!empty($_POST['shop_id']) && !empty($_POST['item_name'])){
            $order = array();
            $order['member_id'] = $_SESSION['MEMBER_ID'];
            $order['shop_id'] = !empty($_POST['shop_id'])?$_POST['shop_id']:"";
            $order['order_status'] = "Pending";
            $order_id = $this->dbapi->addOrder($order);
            if(!empty($_POST['item_name']) && is_array($_POST['item_name'])){
                foreach($_POST['item_name'] as $key=>$item_name){
                    if(!empty($_POST['item_name'][$key])){
                        $pdata = array();
                        $pdata['order_id'] = $order_id;
                        $pdata['item_name'] = $_POST['item_name'][$key];
                        $pdata['item_qty'] = !empty($_POST['item_qty'][$key])?$_POST['item_qty'][$key]:"";
                        $pdata['item_unit'] = !empty($_POST['item_unit'][$key])?$_POST['item_unit'][$key]:"";
                        $this->dbapi->addOrderItem($pdata);
                    }
                }
            }
            $_SESSION['message'] = "Your order has been sent. Shop owner will get back to you soon!";
            redirect(base_url()."my/orders/");
        }
        if (empty($shop_id)) {
            $shop_id = !empty($this->_REQ['shop_id']) ? $this->_REQ['shop_id'] : "";
        }
        if (!empty($shop_id)) {
            $shop = $this->dbapi->getShopById($shop_id);
        }
        if (empty($shop['shop_id'])) {
            redirect(base_url() . "shops/");
        }
        $data['shop'] = $shop;
        if(!empty($this->_REQ['order_id'])){
            $data['order_items'] = $this->dbapi->getOrderItems($this->_REQ['order_id']);
        }
        $data['unit_names'] = ['Kgs', 'Liters', 'Nos'];
        $this->_template("my/purchase", $data);
    }

    public function orders()
    {
        $data = array();
        if (!empty($_GET['act']) && $_GET['act'] == "status" && !empty($_GET['order_id']) && isset($_GET['sta'])) {
            $is_active = (!empty($_GET['sta']) && $_GET['sta'] == "1") ? "1" : "0";
            $this->admin->updateOrder(array("is_active" => $is_active), $_GET['order_id']);
            redirect(base_url() . "my/orders");
        }
        $search_data = array();
        if (!empty($this->_REQ['key'])) {
            $search_data['key'] = $this->_REQ['key'];
        }
        $search_data['member_id'] = $_SESSION['MEMBER_ID'];
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = $search_data;
        $this->pagenavi->per_page = 20;
        $this->pagenavi->base_url = '/my/orders/';
        $this->pagenavi->process($this->dbapi, 'searchOrders');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['orders'] = $this->pagenavi->items;
        $this->_template('my/orders', $data);
    }

    public function order_items(){
        $data = array();
        $data['order'] = $this->dbapi->getOrderById($this->_REQ['order_id']);
        $data['shop'] = $this->dbapi->getShopById($data['order']['shop_id']);
        $data['order_items'] = $this->dbapi->getOrderItems($this->_REQ['order_id']);
        $this->load->view('my/order_items', $data);
    }

    public function shops(){
        $data = array();
        $search_data = array();
        $city_id = "";
        if (!empty($this->_REQ['cat'])) {
            $search_data['category_id'] = $this->_REQ['cat'];
        }
        if (!empty($this->_REQ['city'])) {
            $search_data['city_id'] = $this->_REQ['city'];
            $city_id = $this->_REQ['city'];
        }
        if (!empty($this->_REQ['loc'])) {
            $search_data['location_id'] = $this->_REQ['loc'];
        }
        $this->load->library('Pagenavi');
        $this->pagenavi->search_data = $search_data;
        $this->pagenavi->per_page = 30;
        $this->pagenavi->base_url = '/my/shops/';
        $this->pagenavi->process($this->dbapi, 'searchFavoriteShops');
        $data['PAGING'] = $this->pagenavi->links_html;
        $data['shops'] = $this->pagenavi->items;
        $data['locations'] = [];
        $data['parent_locations'] = $this->dbapi->getLocationsList("0", "1");
        if(!empty($data['parent_locations'])){
            if(empty($city_id)) {
                foreach ($data['parent_locations'] as $loc_id => $loc) {
                    $city_id = $loc_id;
                    break;
                }
            }
            $data['locations'] = $this->dbapi->getLocationsList($city_id, "1");
        }
        $data['categories'] = $this->dbapi->getCategoriesList();
        $this->_template('my/myshops', $data);
    }

    public function add_favorite($shop_id){
        $this->dbapi->addFavoriteShop($shop_id, $_SESSION['MEMBER_ID']);
        redirect(base_url()."shops/");
    }

    public function profile()
    {
        $data = array();
        if (!empty($_POST['name'])) {
            $pdata = array();
            $pdata['name'] = !empty($_POST['name']) ? trim($_POST['name']) : "";
            $pdata['gender'] = !empty($_POST['gender']) ? $_POST['gender'] : "";
            $pdata['mobile'] = !empty($_POST['mobile']) ? trim($_POST['mobile']) : "";
            $pdata['address'] = !empty($_POST['address']) ? trim($_POST['address']) : "";
            $pdata['latitude'] = !empty($_POST['latitude']) ? trim($_POST['latitude']) : "";
            $pdata['longitude'] = !empty($_POST['longitude']) ? trim($_POST['longitude']) : "";
            //$pdata['is_active'] = "1";
            $mobile_exist = $this->dbapi->checkMemberMobile($pdata['mobile'], $_SESSION['MEMBER_ID']);
            if ($mobile_exist) {
                $_SESSION['error'] = "Already registered with this Mobile";
            } else {
                $this->dbapi->updateMember($_SESSION['MEMBER_ID'], $pdata);
                $_SESSION['message'] = "Your Profile has been updated successfully.";
                redirect(base_url() . "my/profile/");
            }
        }
        $data['locations'] = [];
        $data['parent_locations'] = $this->dbapi->getLocationsList("0", "1");
        if (!empty($data['parent_locations'])) {
            foreach ($data['parent_locations'] as $loc_id => $loc) {
                $data['locations'] = $this->dbapi->getLocationsList($loc_id, "1");
                break;
            }
        }
        $data['member'] = $this->dbapi->getMemberById($_SESSION['MEMBER_ID']);
        // $data['addresses'] = $this->dbapi->getAddressByMemberId($_SESSION['MEMBER_ID']);
        $this->_template("my/profile", $data);
    }

    public function logout()
    {
        $_SESSION = array();
        redirect(base_url() . "login/");
    }

}
