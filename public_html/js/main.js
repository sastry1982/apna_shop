/**
 * Main AngularJS Web Application
 */
var app = angular.module('ApnaShopApp', ['ngRoute', 'ngMessages', 'ngAnimate'
]);


app.controller('SignUpController', function () {
    var ctrl = this,
        newCustomer = {name: '', userName: '', password: ''};

    var signup = function () {
        if (ctrl.signupForm.$valid) {
            ctrl.showSubmittedPrompt = true;
            clearForm();
        }
    };

    var clearForm = function () {
        ctrl.newCustomer = {name: '', userName: '', password: ''};
        ctrl.signupForm.$setUntouched();
        ctrl.signupForm.$setPristine();
    };

    var getPasswordType = function () {
        return ctrl.signupForm.showPassword ? 'text' : 'password';
    };

    var toggleEmailPrompt = function (value) {
        ctrl.showEmailPrompt = value;
    };

    var toggleUsernamePrompt = function (value) {
        ctrl.showUsernamePrompt = value;
    };

    var hasErrorClass = function (field) {
        return ctrl.signupForm[field].$touched && ctrl.signupForm[field].$invalid;
    };

    var showMessages = function (field) {
        return ctrl.signupForm[field].$touched || ctrl.signupForm.$submitted
    };

    ctrl.showEmailPrompt = false;
    ctrl.showUsernamePrompt = false;
    ctrl.showSubmittedPrompt = false;
    ctrl.toggleEmailPrompt = toggleEmailPrompt;
    ctrl.toggleUsernamePrompt = toggleUsernamePrompt;
    ctrl.getPasswordType = getPasswordType;
    ctrl.hasErrorClass = hasErrorClass;
    ctrl.showMessages = showMessages;
    ctrl.newCustomer = newCustomer;
    ctrl.signup = signup;
    ctrl.clearForm = clearForm;
});

app.controller('ExampleController', function ($scope) {
    $scope.changeme = function () {

    }
});

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        // Home
        .when("/", {templateUrl: "partials/home.html", controller: "PageCtrl"})

        .when("/login", {templateUrl: "partials/login.html", controller: "PageCtrl"})
        .when("/register", {templateUrl: "partials/register.html", controller: "PageCtrl"})
    ;
}]);

/**
 * Controls all other Pages
 */
app.controller('PageCtrl', function ($scope, $location /*$http */) {

    console.log("Page Controller reporting for duty.");
    $scope.go = function (path) {
        $location.path(path);
    };


    // Activates the Carousel
    $('.carousel').carousel({
        interval: 5000
    });

    // Activates Tooltips for Social Links
    $('.tooltip-social').tooltip({
        selector: "a[data-toggle=tooltip]"
    })
});